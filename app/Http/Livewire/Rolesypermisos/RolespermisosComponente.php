<?php

namespace App\Http\Livewire\Rolesypermisos;

use Livewire\Component;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Livewire\WithPagination;



class RolespermisosComponente extends Component
{
    use WithPagination;
    //Propiedades de Los campos
    public $permisoTitle = "Crear", $roleTitle = "Crear", $userSelected;
    public $tab = "roles", $roleSelected;

    //Itdyaingenieria Software
    public function render()
    {
        $roles = Role::select('*', DB::RAW("0 as checked"))->get();
        $permisos = Permission::select('*', DB::RAW("0 as checked"))->get();
        // dd($roles);
        if ($this->userSelected != '' && $this->userSelected != 'Seleccionar') {
            foreach ($roles as $r) {
                $user = User::find($this->userSelected);
                $tieneRole = $user->hasRole($r->name);
                if ($tieneRole) {
                    $r->checked = 1;
                }
            }
        }
        if ($this->roleSelected != '' && $this->roleSelected != 'Seleccionar') {
            foreach ($permisos as $p) {
                $rol = Role::find($this->roleSelected);
                $tienePermiso = $rol->hasPermissionTo($p->name);
                if ($tienePermiso) {
                    $p->checked = 1;
                }
            }
        }
        return view(
            'livewire.rolesypermisos.listado',
            [
                "roles" => $roles,
                "permisos" => $permisos,
                "usuarios" => User::select('id', 'name')->get(),

            ]
        );
    }

    //Seccion de Roles
    public function resetInput()
    {
        $this->roleTitle = 'Crear';
        $this->permisoTitle = 'Crear';
        $this->userSelected = '';
        $this->roleSelected = '';
    }

    public function crearRol($rolName, $rolId)
    {
        if ($rolId) {
            $this->UpdateRol($rolName, $rolId);
        } else {
            $this->SaveRol($rolName);
        }
    }

    public function SaveRol($rolName)
    {
        $rol = Role::where('name', $rolName)->first();
        if ($rol) {
            session()->flash('message', "El Rol ya está en Sistema.!!");
            return;
        }
        Role::create([
            'name' => $rolName,
        ]);
        session()->flash('message', "El Rol: " . $rolName . " se creo Exitosamente en Sistema.!!");
        $this->resetInput();
    }

    public function UpdateRol($rolName, $rolId)
    {

        $rol = Role::where('name', $rolName)->where('id', '<>', $rolId)->first();
            if ($rol) {
            session()->flash('message', "El Rol ya está en Sistema.!!");
            return;
        }

        $rol = Role::find($rolId);
      // dd($rol);
        $rol->name = $rolName;
        $rol->save();
        session()->flash('message', "El Rol: " . $rolName . " se Actualizó Exitosamente en Sistema.!!");
        //dd(session('message'));
        $this->resetInput();
    }

    public function destroyRol($rolId)
    {
        $rol = Role::find($rolId);
        $rol->delete();
        session()->flash('message', "El Rol se Eliminó Exitosamente en Sistema.!!");
        $this->resetInput();
    }

    public function AsignarRoles($rolesList)
    {
        if ($this->userSelected) {
            $user = User::find($this->userSelected);
            if ($user) {
                $user->syncRoles($rolesList); //Asignamos los roles o sincronizamos
                session()->flash('message', "Roles Asigandos Exitosamente.!!");
                $this->resetInput();
            }
        }
    }
    //Listeners de escucha de eventos y ejecucion de acciones
    protected $listeners = [
        'destroyRol' => 'destroyRol',
        'destroyPermiso' => 'destroyPermiso',
        'crearPermiso' => 'crearPermiso',
        'crearRol' => 'crearRol',
        'AsignarRoles' => 'AsignarRoles',
        'AsignarPermisos' => 'AsignarPermisos',
    ];

    //seccion de los Permisos-Itdyaingenieria

    public function crearPermiso($permisoName, $permisoId)
    {
        if ($permisoId) {
            $this->UpdatePermiso($permisoName, $permisoId);
        } else {
            $this->SavePermiso($permisoName);
        }
    }

    public function SavePermiso($permisoName)
    {
        $permiso = Permission::where('name', $permisoName)->first();
        if ($permiso) {
            session()->flash('message', "El permiso ya está en Sistema.!!");
            return;
        }
        Permission::create([
            'name' => $permisoName,
        ]);
        session()->flash('message', "El Permiso: " . $permisoName . " se creo Exitosamente en Sistema.!!");
        $this->resetInput();
    }

    public function UpdatePermiso($permisoName, $permisoId)
    {
        $permiso = Permission::where('name', $permisoName)->where('id', '<>', $permisoId)->first();
        if ($permiso) {
            session()->flash('message', "El Permiso ya está en Sistema.!!");
            return;
        }

        $permiso = Permission::find($permisoId);
        $permiso->name = $permisoName;
        $permiso->save();
        session()->flash('message', "El Permiso: " . $permisoName . " se Actualizó Exitosamente en Sistema.!!");
        $this->resetInput();
    }

    public function destroyPermiso($permisoId)
    {
        $permiso = Permission::find($permisoId);
        $permiso->delete();
        session()->flash('message', "El Permiso se Eliminó Exitosamente en Sistema.!!");
        $this->resetInput();
    }

    public function AsignarPermisos($permisosList, $rolId)
    {
        if ($rolId > 0) {
            $rol = Role::find($rolId);
            if ($rol) {
                $rol->syncPermissions($permisosList);
                session()->flash('message', "Permisos Asignados Exitosamente en Sistema.!!");
                $this->resetInput();
            }
        }
    }
}
