<?php

namespace App\Http\Livewire\Catalogos\vehiculos;

use App\Models\VehiculosModel;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;


class VehiculosComponente extends Component
{
    use WithPagination;
    //Propiedades de Los campos
    public $vehiculo_id, $placa, $marca, $modelo, $tipocombustible, $empresa_id;
    //Propiedades de Otras Acciones
    public $accion = "crea";
    public $isOpen = 0; //para llamar a la vista con el Form Modal
    public $buscar; //Propiedad para busqueda

    protected $rules = [
        'placa' => 'required',
    ];

    protected $messages = [
        'marca.required' => 'La Marca, es necesaria!!',
        'placa.unique' => 'Esta Placa, ya se encuentra, Registrada.!!',
        'placa.required' => 'Placa es necesaria. Verifique.!!',
        'modelo.required' => 'Modelo, es necesario. Verifique.!!',
        'tipocombustible.required' => 'Tipo Combustible, necesario. Verifique.!!',
    ];

    protected $queryStrings = [
        'buscar' => ['except' => ''],
    ];

    //Itdyaingenieria Software

    public function updatingBuscar()
    {
        $this->resetPage();
    }

    public function render()
    {
        $vehiculoss = DB::table('vehiculos as veh')
            ->leftjoin('empresa as e', 'e.empresa_id', 'veh.empresa_id')
            ->select(
                'veh.*',
                'e.nombre'
            )
            ->where('veh.placa', 'LIKE', '%' . $this->buscar . '%')
            ->orwhere('veh.marca', 'LIKE', '%' . $this->buscar . '%')
            ->latest('veh.placa')
            ->paginate(10);

        return view(
            'livewire.catalogos.vehiculos.vehiculos',
            [
                "vehiculoss" => $vehiculoss,
            ]
        );
    }

    public function llamarModalCrear()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->isOpen = false;
    }

    public function creaVehiculo()
    {
        //Validamos la Propiedades del componente
        $this->validate([
            'placa' => 'required|unique:vehiculos',
            'marca' => 'required',
            'modelo' => 'required',
            'tipocombustible' => 'required',
        ], $this->messages);

        $vehiculoss = new VehiculosModel;
        $vehiculoss->placa = $this->placa;
        $vehiculoss->marca = $this->marca;
        $vehiculoss->modelo = $this->modelo;
        $vehiculoss->tipocombustible = $this->tipocombustible;
        $vehiculoss->empresa_id = 1;
        $vehiculoss->save();

        //Reseteamos los valores de las propiedades y cerramos el Modal
        $this->closemodal();
        session()->flash('message', "El Registro se Grabó Exitosamente.!!");
    }

    public function actualizaVehiculo()
    {

        $this->validate([
            'placa' => 'required',
            'marca' => 'required',
            'modelo' => 'required',
            'tipocombustible' => 'required',
        ], $this->messages);
        $vehiculoss = VehiculosModel::findOrFail($this->vehiculo_id);
        $vehiculoss->placa = $this->placa;
        $vehiculoss->marca = $this->marca;
        $vehiculoss->modelo = $this->modelo;
        $vehiculoss->tipocombustible = $this->tipocombustible;
        $vehiculoss->Update();

        //Reseteamos los valores de las propiedades
        session()->flash('message', "El Registro: " . $this->placa . ", se Actualizó Exitosamente.!!");
        $this->closemodal();
    }

    function default()
    {
        $this->resetErrorBag();
        $this->resetValidation();

        //Reseteamos los valores de las propiedades
        $this->reset([
            'vehiculo_id', 'placa', 'marca', 'modelo', 'tipocombustible', 'empresa_id', 'accion',
        ]);
    }

    public function confirmarItemAdicionar()
    {
        $this->default();
        $this->isOpen = false;
    }

    public function confirmarItemEditar(VehiculosModel $elVehiculo, $elid)
    {
        $this->llamarModalCrear(); //Llamamos el form modal con los datos del registro
        $this->vehiculo_id = $elid;
        $this->placa = $elVehiculo->placa;
        $this->marca = $elVehiculo->marca;
        $this->modelo = $elVehiculo->modelo;
        $this->tipocombustible = $elVehiculo->tipocombustible;
        $this->accion = 'actualiza';
    }


    public function eliminaVehiculo($elVehiculo)
    {
        $this->validasc = DB::table('vehiculos as v')
            ->leftjoin('conductores as c', 'v.vehiculo_id', 'c.vehiculo_id')
            ->select('c.nombres', 'v.idvehiculo')
            ->where('v.vehiculo_id', '=', $elVehiculo)
            ->count();
        //Si no Existen Vehiculos con movimiento en la hoja de Vida, procedemos a Eliminar
        if ($this->validasc <= 0) {
            $vehiculoss = VehiculosModel::findOrFail($elVehiculo);
            $vehiculoss->delete();
        }
        //Enviamos la cantidad de Vehiculos con Movimiento
        $this->emit('cantidad', $this->validasc);
    }
    //Listeners de escucha de eventos y ejecucion de acciones
    protected $listeners = [
        'deleteRow' => 'eliminaVehiculo',
    ];
}
