<?php

namespace App\Http\Livewire\Catalogos\clientes;

use App\Models\ClientesModel;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;


class ClientesComponente extends Component
{
    use WithPagination;
    //Propiedades de Los campos
    public $cliente_id, $numeroIdenficacion, $nombres, $apellidos,
        $direccion, $telefono, $email, $empresa_id;
    //Propiedades de Otras Acciones
    public $accion = "crea";
    public $isOpen = 0; //para llamar a la vista con el Form Modal
    public $buscar; //Propiedad para busqueda

    protected $rules = [
        'numeroIdenficacion' => 'required',
    ];

    protected $messages = [
        'nombres.required' => 'El Nombre Cliente, es necesario!!',
        'numeroIdenficacion.required' => 'Numero Identificación, es necesaria!!',
        'numeroIdenficacion.unique' => 'Esta Identificación, ya se encuentra, Registrada.!!',
        'direccion.required' => 'Dirección, es necesario. Verifique.!!',
        'telefono.required' => 'Telefono Cliente, necesario. Verifique.!!',
        'email.unique' => 'Este Email ya esta Registrado. Verifique.!!',
        'email.required' => 'El Email,es necesario. Verifique.!!',
    ];

    protected $queryStrings = [
        'buscar' => ['except' => ''],
    ];

    //Itdyaingenieria Software
    public function updatingBuscar()
    {
        $this->resetPage();
    }

    public function render()
    {
        $clientess = DB::table('clientes as c')
            ->select(
                'c.*',
            )
            ->where('c.nombres', 'LIKE', '%' . $this->buscar . '%')
            ->orwhere('c.direccion', 'LIKE', '%' . $this->buscar . '%')
            ->latest('c.nombres')
            ->paginate(10);
        return view(
            'livewire.catalogos.clientes.clientes',
            [
                "clientess" => $clientess,
            ]
        );
    }

    public function llamarModalCrear()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->isOpen = false;
    }

    public function creaCliente()
    {
        //Validamos la Propiedades del componente
        $this->validate([
            'nombres' => 'required',
            'numeroIdenficacion' => 'required',
            'numeroIdenficacion' => 'required|unique:clientes',
            'direccion' => 'required',
            'telefono' => 'required',
            'email' => 'required|unique:clientes',
        ], $this->messages);

        $clientess = new ClientesModel;
        $clientess->numeroIdenficacion = $this->numeroIdenficacion;
        $clientess->nombres = $this->nombres;
        $clientess->apellidos = $this->apellidos;
        $clientess->direccion = $this->direccion;
        $clientess->telefono = $this->telefono;
        $clientess->email = $this->email;
        $clientess->empresa_id = 1;
        $clientess->save();

        //Reseteamos los valores de las propiedades y cerramos el Modal
        $this->closemodal();
        session()->flash('message', "El Registro se Grabó Exitosamente.!!");
    }

    public function actualizaCliente()
    {

        $this->validate([
            'nombres' => 'required',
            'numeroIdenficacion' => "required|unique:clientes,numeroIdenficacion,{$this->cliente_id},cliente_id",
            'direccion' => 'required',
            'telefono' => 'required',
            'email' => "required|unique:clientes,email,{$this->cliente_id},cliente_id",
        ], $this->messages);

        $clientess = ClientesModel::findOrFail($this->cliente_id);
        // $clientess->cliente_id = $this->idperso; //Llave primaria no actualizable
        $clientess->numeroIdenficacion = $this->numeroIdenficacion;
        $clientess->nombres = $this->nombres;
        $clientess->apellidos = $this->apellidos;
        $clientess->direccion = $this->direccion;
        $clientess->telefono = $this->telefono;
        $clientess->Update();

        //Reseteamos los valores de las propiedades
        session()->flash('message', "El Registro: " . $this->nombres . " " . $this->apellidos . ", se Actualizó Exitosamente.!!");
        $this->closemodal();
    }

    function default()
    {
        $this->resetErrorBag();
        $this->resetValidation();

        //Reseteamos los valores de las propiedades
        $this->reset([
            'cliente_id', 'numeroIdenficacion', 'nombres',
            'apellidos', 'direccion', 'telefono', 'email', 'empresa_id',
        ]);
    }

    public function confirmarItemAdicionar()
    {
        $this->default();
        $this->isOpen = false;
    }

    public function confirmarItemEditar(ClientesModel $elCliente, $elid)
    {
        $this->llamarModalCrear(); //Llamamos el form modal con los datos del registro
        $this->cliente_id = $elid;
        $this->numeroIdenficacion = $elCliente->numeroIdenficacion;
        $this->nombres = $elCliente->nombres;
        $this->apellidos = $elCliente->apellidos;
        $this->direccion = $elCliente->direccion;
        $this->telefono = $elCliente->telefono;
        $this->email = $elCliente->email;
        $this->accion = 'actualiza';
    }

    public function eliminaCliente($elCliente)
    {
        $clientess = ClientesModel::findOrFail($elCliente);
        $clientess->delete();
    }
    //Listeners de escucha de eventos y ejecucion de acciones
    protected $listeners = [
        'deleteRow' => 'eliminaCliente',
    ];
}
