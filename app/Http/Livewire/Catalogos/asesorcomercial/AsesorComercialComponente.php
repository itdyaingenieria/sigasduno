<?php

namespace App\Http\Livewire\Catalogos\asesorcomercial;

use App\Models\AsesoresComercialesModel;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;


class AsesorComercialComponente extends Component
{
    use WithPagination;
    //Propiedades de Los campos
    public $asesor_id, $numeroIdentificacion, $nombres,
        $apellidos, $antiguedad, $tipo, $fechaIngreso, $direccion;
    //Propiedades de Otras Acciones
    public $accion = "crea";
    public $isOpen = 0; //para llamar a la vista con el Form Modal
    public $buscar; //Propiedad para busqueda

    protected $rules = [
        'numeroIdenficacion' => 'required',
    ];

    protected $messages = [
        'nombres.required'              => 'El Nombre Asesor, es necesario!!',
        'numeroIdentificacion.required' => 'Numero Identificación, es necesaria!!',
        'numeroIdenficacion.unique'     => 'Esta Identificación, ya se encuentra, Registrada.!!',
        'direccion.required'            => 'Dirección, es necesario. Verifique.!!',
        'fechaIngreso.required'         => 'Ingreso es, necesario. Verifique.!!',
    ];

    protected $queryStrings = [
        'buscar' => ['except' => ''],
    ];

    //Itdyaingenieria Software
    public function updatingBuscar()
    {
        $this->resetPage();
    }

    public function render()
    {
        $asesores = DB::table('asesorcomercial as a')
            ->select(
                'a.*',
            )
            ->where('a.nombres', 'LIKE', '%' . $this->buscar . '%')
            ->orwhere('a.direccion', 'LIKE', '%' . $this->buscar . '%')
            ->latest('a.nombres')
            ->paginate(10);
        return view(
            'livewire.catalogos.asesorcomercial.asesor',
            [
                "asesores" => $asesores,
            ]
        );
    }

    public function llamarModalCrear()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->isOpen = false;
    }

    public function creaCliente()
    {
        //Validamos la Propiedades del componente
        $this->validate([
            'nombres' => 'required',
            'numeroIdentificacion' => 'required',
            'numeroIdentificacion' => 'required|unique:asesorcomercial',
            'direccion' => 'required',
            'fechaIngreso' => 'required',
        ], $this->messages);

        $asesores = new AsesoresComercialesModel;
        $asesores->numeroIdentificacion = $this->numeroIdentificacion;
        $asesores->nombres = $this->nombres;
        $asesores->apellidos = $this->apellidos;
        $asesores->antiguedad = $this->antiguedad;
        $asesores->tipo = $this->tipo;
        $asesores->fechaIngreso = $this->fechaIngreso;
        $asesores->direccion = $this->direccion;
        $asesores->save();

        //Reseteamos los valores de las propiedades y cerramos el Modal
        $this->closemodal();
        session()->flash('message', "El Registro se Grabó Exitosamente.!!");
    }

    public function actualizaCliente()
    {

        $this->validate([
            'nombres' => 'required',
            'numeroIdentificacion' => "required|unique:asesorcomercial,numeroIdentificacion,{$this->asesor_id},asesor_id",
            'direccion' => 'required',
            'fechaIngreso' => 'required',
        ], $this->messages);

        $asesores = AsesoresComercialesModel::findOrFail($this->asesor_id);
        $asesores->numeroIdentificacion = $this->numeroIdentificacion;
        $asesores->nombres = $this->nombres;
        $asesores->apellidos = $this->apellidos;
        $asesores->antiguedad = $this->antiguedad;
        $asesores->tipo = $this->tipo;
        $asesores->fechaIngreso = $this->fechaIngreso;
        $asesores->direccion = $this->direccion;
        $asesores->Update();

        //Reseteamos los valores de las propiedades
        session()->flash('message', "El Registro: " . $this->nombres . " " . $this->apellidos . ", se Actualizó Exitosamente.!!");
        $this->closemodal();
    }

    function default()
    {
        $this->resetErrorBag();
        $this->resetValidation();

        //Reseteamos los valores de las propiedades
        $this->reset([
            'asesor_id', 'numeroIdentificacion', 'nombres',
            'apellidos', 'antiguedad', 'tipo', 'fechaIngreso', 'direccion', 'accion',
        ]);
    }

    public function confirmarItemAdicionar()
    {
        $this->default();
        $this->isOpen = false;
    }

    public function confirmarItemEditar(AsesoresComercialesModel $elAsesor, $elid)
    {
        $this->llamarModalCrear(); //Llamamos el form modal con los datos del registro
        $this->asesor_id = $elid;
        $this->numeroIdentificacion = $elAsesor->numeroIdentificacion;
        $this->nombres = $elAsesor->nombres;
        $this->apellidos = $elAsesor->apellidos;
        $this->antiguedad = $elAsesor->antiguedad;
        $this->tipo = $elAsesor->tipo;
        $this->fechaIngreso = $elAsesor->fechaIngreso;
        $this->direccion = $elAsesor->direccion;
        $this->accion = 'actualiza';
    }

    public function eliminaCliente($elAsesor)
    {
        $asesores = AsesoresComercialesModel::findOrFail($elAsesor);
        $asesores->delete();
    }
    //Listeners de escucha de eventos y ejecucion de acciones
    protected $listeners = [
        'deleteRow' => 'eliminaCliente',
    ];
}
