<?php

namespace App\Http\Livewire\Catalogos\codperso;

use App\Models\CodpersoModel;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;


class CodpersoComponente extends Component
{
    //Conductores
    use WithPagination;
    //Propiedades de Los campos
    public $conductor_id, $nombres, $apellidos, $direccion, $tipoLicencia, $numeroLicencia, $email, $telefono, $vehiculo_id;
    //Propiedades de Otras Acciones
    public $accion = "crea";
    public $isOpen = 0; //para llamar a la vista con el Form Modal
    public $buscar; //Propiedad para busqueda

    protected $rules = [
        'conductor_id' => 'required',
    ];

    protected $messages = [
        'nombres.required' => 'Nombres, es necesario!!',
        'apellidos.required' => 'Apellidos, es necesario!!',
        'direccion.required' => 'Direccion, es necesario. Verifique.!!',
        'tipoLicencia.required' => 'Tipo Licencia, es necesario. Verifique.!!',
        'numeroLicencia.required' => 'Numero Licencia, es necesario. Verifique.!!',
        'numeroLicencia.unique' => 'Este Numero, ya se encuentra, Registrado.!!',
        'email.required' => 'El Email, es necesario. Verifique.!!',
        'email.unique' => 'Este Email, ya se encuentra, Registrado.!!',
        'telefono.required' => 'Teléfono tercero, es necesario. Verifique.!!',
        'vehiculo_id.required' => 'El Vehiculo, es necesario. Verifique.!!',
    ];

    protected $queryStrings = [
        'buscar' => ['except' => ''],
    ];

    //Itdyaingenieria Software

    public function updatingBuscar()
    {
        $this->resetPage();
    }

    public function render()
    {
        $conductoress = DB::table('conductores as c')
            ->leftjoin('vehiculos as v', 'c.vehiculo_id', 'v.vehiculo_id')
            ->select(
                'c.*',
                'v.placa'
            )
            ->where('c.nombres', 'LIKE', '%' . $this->buscar . '%')
            ->orwhere('c.numeroLicencia', 'LIKE', '%' . $this->buscar . '%')
            ->latest('c.nombres')
            ->paginate(10);

        $vehiculos = DB::table('vehiculos')
            ->orderBy('placa')
            ->get();

        return view(
            'livewire.catalogos.codperso.codperso',
            [
                "conductoress" => $conductoress,
                "vehiculos" => $vehiculos,
            ]
        );
    }

    public function llamarModalCrear()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->isOpen = false;
    }

    public function creaTercero()
    {
        //Validamos la Propiedades del componente
        $this->validate([
            'nombres' => 'required',
            'apellidos' => 'required',
            'direccion' => 'required',
            'tipoLicencia' => 'required',
            'numeroLicencia' => 'required|unique:conductores',
            'email' => 'unique:conductores',
            'vehiculo_id' => 'required',
        ], $this->messages);

        $terceross = new CodpersoModel;
        $terceross->nombres = $this->nombres;
        $terceross->apellidos = $this->apellidos;
        $terceross->direccion = $this->direccion;
        $terceross->tipoLicencia = $this->tipoLicencia;
        $terceross->numeroLicencia = $this->numeroLicencia;
        $terceross->email = $this->email;
        $terceross->telefono = $this->telefono;
        $terceross->vehiculo_id = $this->vehiculo_id;

        $terceross->save();

        //Reseteamos los valores de las propiedades y cerramos el Modal
        $this->closemodal();
        session()->flash('message', "El Registro se Grabó Exitosamente.!!");
    }

    public function actualizaTercero()
    {

        $this->validate([
            'nombres' => 'required',
            'apellidos' => 'required',
            'direccion' => 'required',
            'tipoLicencia' => 'required',
            'numeroLicencia' => "required|unique:conductores,numeroLicencia,{$this->conductor_id},conductor_id",
            'email' => "required|unique:conductores,email,{$this->conductor_id},conductor_id",
            'vehiculo_id' => 'required',
        ], $this->messages);

        $terceross = CodpersoModel::findOrFail($this->conductor_id);
        $terceross->nombres = $this->nombres;
        $terceross->apellidos = $this->apellidos;
        $terceross->direccion = $this->direccion;
        $terceross->tipoLicencia = $this->tipoLicencia;
        $terceross->numeroLicencia = $this->numeroLicencia;
        $terceross->email = $this->email;
        $terceross->telefono = $this->telefono;
        $terceross->vehiculo_id = $this->vehiculo_id;

        $terceross->Update();

        //Reseteamos los valores de las propiedades
        session()->flash('message', "El Registro: " . $this->nombres . " " . $this->apellidos . ", se Actualizó Exitosamente.!!");
        $this->closemodal();
    }

    function default()
    {
        $this->resetErrorBag();
        $this->resetValidation();

        //Reseteamos los valores de las propiedades
        $this->reset([
            'conductor_id', 'nombres', 'apellidos', 'direccion', 'tipoLicencia',
            'numeroLicencia', 'email', 'telefono', 'vehiculo_id', 'accion',
        ]);
    }

    public function confirmarItemAdicionar()
    {
        $this->default();
        $this->isOpen = false;
    }

    public function confirmarItemEditar(CodpersoModel $elTercero, $elid)
    {
        $this->llamarModalCrear(); //Llamamos el form modal con los datos del registro
        $this->conductor_id = $elid;
        $this->nombres = $elTercero->nombres;
        $this->apellidos = $elTercero->apellidos;
        $this->direccion = $elTercero->direccion;
        $this->tipoLicencia = $elTercero->tipoLicencia;
        $this->numeroLicencia = $elTercero->numeroLicencia;
        $this->email = $elTercero->email;
        $this->telefono = $elTercero->telefono;
        $this->vehiculo_id = $elTercero->vehiculo_id;

        $this->accion = 'actualiza';
    }


    public function eliminaTercero($elTercero)
    {
        $this->validasc = DB::table('conductores as c')
            ->leftjoin('vehiculos as v', 'c.vehiculo_id', 'v.vehiculo_id')
            ->select('c.conductor_id', 'v.vehiculo_id')
            ->where('c.conductor_id', '=', $elTercero)
            ->count();
        //Si no Existen codperso con movimiento en la hoja de Vida, procedemos a Eliminar
        if ($this->validasc <= 0) {
            $terceross = CodpersoModel::findOrFail($elTercero);
            $terceross->delete();
        }
        //Enviamos la cantidad de Vehiculos con Movimiento
        $this->emit('cantidad', $this->validasc);
    }

    //Listeners de escucha de eventos y ejecucion de acciones
    protected $listeners = [
        'deleteRow' => 'eliminaTercero',
    ];
}
