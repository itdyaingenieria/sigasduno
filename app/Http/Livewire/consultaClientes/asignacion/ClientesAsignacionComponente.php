<?php

namespace App\Http\Livewire\consultaClientes\asignacion;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;


class ClientesAsignacionComponente extends Component
{
    use WithPagination;
    //Propiedades de Los campos
    public $iduser, $idcliente, $name, $email, $email_verified_at, $password, $two_factor_secret,
        $two_factor_recovery_codes, $remember_token, $current_team_id, $profile_photo_path, $created_at, $updated_at;
    //Propiedades de Otras Acciones
    public $accion = "crea";
    public $isOpen = 0; //para llamar a la vista con el Form Modal
    public $buscar; //Propiedad para busqueda

    protected $messages = [
        'idcliente.required' => 'El Nombre Cliente, es necesario!!',
        'idcliente.unique' => 'El Cliente Ya está Asignado, Verifique!!',
        'name.required' => 'El Nombre usuario, es necesario!!',
        'email.unique' => 'Este Email ya esta Registrado. Verifique.!!',
        'email.required' => 'El Email,es necesario. Verifique.!!',
        'password.required' => 'La Contraseña es necesario. Verifique.!!',
    ];

    protected $queryStrings = [
        'buscar' => ['except' => ''],
    ];

    //Itdyaingenieria Software
    public function updatingBuscar()
    {
        $this->resetPage();
    }

    public function render()
    {
        $userss = DB::table('users as u')
            ->rightjoin('clientes as c', 'u.idcliente', 'c.idcliente')
            ->select(
                'u.*',
                'c.clinmbre',
                'c.clidrccion',
            )
            ->where('c.clinmbre', 'LIKE', '%' . $this->buscar . '%')
            ->whereNotNull('u.name')
            ->latest('c.clinmbre')
            ->paginate(10);

        $clientess = DB::table('clientes')
            ->orderBy('clinmbre')
            ->get();

        return view(
            'livewire.consultasClientes.clientesAsignacion.asignacion',
            [
                "userss" => $userss,
                "clientess" => $clientess,
            ]
        );
    }

    public function llamarModalCrear()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->isOpen = false;
    }

    public function creaCliente()
    {
        //Validamos la Propiedades del componente
        $this->validate([
            'idcliente' => 'required|unique:users',
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
        ], $this->messages);

        $clientess = new User;
        $clientess->idcliente = $this->idcliente;
        $clientess->name = $this->name;
        $clientess->email = $this->email;
        $clientess->password = $this->password;
        $clientess->save();

        //Reseteamos los valores de las propiedades y cerramos el Modal
        $this->closemodal();
        session()->flash('message', "El Registro se Grabó Exitosamente.!!");
    }

    public function actualizaCliente()
    {
        $this->validate([
            'idcliente' => "required|unique:users,idcliente,{$this->iduser},id",
            'name' => 'required',
            'email' => "required|unique:users,email,{$this->iduser},id",
            'password' => "required:users,password,{$this->iduser},id",
        ], $this->messages);

        $clientess = User::findOrFail($this->iduser);
        // $clientess->id = $this->iduser; //Llave primaria no actualizable
        $clientess->idcliente = $this->idcliente;
        $clientess->name = $this->name;
        $clientess->email = $this->email;
        $clientess->password = $this->password;
        $clientess->Update();

        //Reseteamos los valores de las propiedades
        session()->flash('message', "El Registro: " . $this->name . ", se Actualizó Exitosamente.!!");
        $this->closemodal();
    }

    function default()
    {
        $this->resetErrorBag();
        $this->resetValidation();

        //Reseteamos los valores de las propiedades
        $this->reset([
            'iduser', 'idcliente', 'name', 'email', 'email_verified_at',
            'password', 'two_factor_secret',
            'two_factor_recovery_codes', 'remember_token',
            'current_team_id', 'profile_photo_path', 'created_at', 'updated_at', 'accion',
        ]);
    }

    public function confirmarItemAdicionar()
    {
        $this->default();
        $this->isOpen = false;
    }

    public function confirmarItemEditar(User $elCliente, $elid)
    {

        $this->llamarModalCrear(); //Llamamos el form modal con los datos del registro
        $this->iduser = $elid;
        $this->idcliente = $elCliente->idcliente;
        $this->name = $elCliente->name;
        $this->email = $elCliente->email;
        $this->password = $elCliente->password;
        $this->accion = 'actualiza';
    }

    public function eliminaCliente($elCliente)
    {
        $eliminaClienteAsignado = User::findOrFail($elCliente);
        $eliminaClienteAsignado->delete();
        session()->flash('message', "El CLiente:, se Eliminó, Exitosamente.!!");
    }
    
    //Listeners de escucha de eventos y ejecucion de acciones
    protected $listeners = [
        'deleteRow' => 'eliminaCliente',
    ];
}
