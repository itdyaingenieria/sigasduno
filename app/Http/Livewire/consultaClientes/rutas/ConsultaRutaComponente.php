<?php

namespace App\Http\Livewire\consultaClientes\rutas;

use App\Models\ControlesRutasModel;
use App\Models\ReportesRutasModel;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;
use Livewire\Component;
use Livewire\WithPagination;
use App\Exports\ControlRutasxClienteExport;



class ConsultaRutaComponente extends Component
{
    use WithPagination;
    //Propiedades de Los campos de Contoles
    public  $idcontrolruta, $fechadespacho, $idhojavida, $idcliente, $placatrailer, $viaje,
        $origen, $destino, $manfcarga, $iduser, $fupdate, $iduser_update, $estado;
    //Propiedades de Los campos de Reportes
    public $idreporteruta, $fechareporte, $reporte, $fotogps, $observacion,
        $estadoreporte, $iduser_reporte, $fupdate_reporte, $iduser_update_reporte;
    //Propiedades de Otras Acciones
    public $fechadesde, $fechahasta, $laplaca;
    public $accion = "crea";
    public $accion2 = "crea";
    public $isOpen = 0; //para llamar a la vista con el Form Modal
    public $isOpenReporte = 0; //para llamar a la vista con el Form Modal Reporte
    public $isOpenReporteRuta = 0; //para llamar a la vista con el Form Modal Reporte de la ruta Gps o Satelital
    public $isOpenCreaReporteRuta = 0; //para llamar a la vista con el Form Create Reporte de la ruta Gps o Satelital
    public $buscar; //Propiedad para busqueda
    public $losreportes = [];
    public $satelital, $usuario, $clave;

    public $lafotogps = null; //para guardar la ruta de Foto GPS/Satelital


    protected $rules = [
        'idhojavida' => 'required',
    ];

    protected $messages = [
        'idhojavida.required' => 'La Hoja de Vida, es necesario!!',
        'idcliente.required' => 'El Cliente, es necesario!!',
        'fechadespacho.required' => 'Fecha es necesaria. Verifique.!!',
        'placatrailer.required' => 'Dato es necesario. Verifique.!!',
        'viaje.unique' => 'Este Viaje, ya se encuentra, Registrado Verifique.!!',
        'viaje.required' => 'Dato es necesario. Verifique.!!',
        'origen.required' => 'Origen es necesario. Verifique.!!',
        'destino.required' => 'Destino es necesario. Verifique.!!',
        'manfcarga.required' => 'Maniefiesto, es necesario. Verifique.!!',
        'manfcarga.unique' => 'Este Manifiesto, ya se encuentra, Registrado.!!',
        'fechareporte.required' => 'Fecha Reporte, es necesario. Verifique.!!',
        'reporte.required' => 'Datos de Reporte, es necesario. Verifique.!!',
        'observacion.required' => 'La Observacion, es necesario. Verifique.!!',
        'lafotogps.image' => 'No es Una Imagen Válida. Verifique.!!',
        'lafotogps.max' => 'La Imagen Tiene mas de 200kb Muy Grande. Verifique.!!',
    ];

    protected $queryStrings = [
        'buscar' => ['except' => ''],
    ];

    //Itdyaingenieria Software

    public function updatingBuscar()
    {
        $this->resetPage();
    }

    public function render()
    {
        //Encabezado de COntrol de Rutas - Maestro
        if (!is_null(auth()->user()->idcliente)) {
            $controless = DB::table('controlesrutas as c')
                ->leftjoin('hojavidas as hv', 'c.idhojavida', 'hv.idhojavida')
                ->leftJoin('vehiculos as v', 'hv.idvehiculo', 'v.idvehiculo')
                ->leftJoin('codperso as conductor', 'hv.idperso_conductor', 'conductor.idperso')
                ->leftJoin('clientes as cl', 'c.idcliente', 'cl.idcliente')
                ->leftjoin('users as u', 'c.idcliente', 'u.idcliente')
                ->select(
                    'c.*',
                    'v.vehplaca',
                    'v.satelital',
                    'v.usuario',
                    'v.clave',
                    'hv.idvehiculo',
                    'conductor.fvencelic',
                    'cl.clinmbre',
                    DB::raw('CONCAT(conductor.nmbrepers, " ", conductor.priapellid," ", IFNULL(conductor.seguapelli,"")) AS elconductor'),
                )
                ->where('v.vehplaca', 'LIKE', '%' . $this->buscar . '%')
                ->where('c.idcliente', auth()->user()->idcliente)
                ->where('c.manfcarga', 'LIKE', '%' . $this->buscar . '%')
                ->latest('c.idcontrolruta')
                ->paginate(10);

            // dd($controless);


            $hojavidass = DB::table('hojavidas as hv')
                ->leftJoin('vehiculos as v', 'hv.idvehiculo', 'v.idvehiculo')
                ->leftJoin('codperso as conductor', 'hv.idperso_conductor', 'conductor.idperso')
                ->select(
                    'hv.*',
                    'v.vehplaca',
                    DB::raw('CONCAT(conductor.nmbrepers, " ", conductor.priapellid," ", IFNULL(conductor.seguapelli,"")) AS elconductor'),
                )
                ->where('hv.estado', '=', 'AUTORIZADO')
                ->orWhere('hv.estadohabilitacion', '=', 'SIN HABILITACION')
                ->where('hv.estadohabilitacion', '=', 'AUTORIZADO')
                ->orderBy('v.vehplaca')
                ->get();

            $clientess = DB::table('clientes as c')
                ->select('c.*',)
                ->where('c.estado', '=', 0)
                ->orderBy('c.clinmbre')
                ->get();


            return view(
                'livewire.consultasClientes.consultaRutas.controlR',
                [
                    "controless" => $controless,
                    "hojavidass" => $hojavidass,
                    "clientess" => $clientess,
                ]
            );
        }

        return view(
            'livewire.consultasClientes.consultaRutas.noCliente'
        );
    }


    public function llamarModalCrear()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->openModal();
    }
    public function llamarModalReporte()
    {
        $this->openModalReporte();
    }

    public function llamarModalReporteRuta()
    {
        $this->openModalReporteRuta();
    }

    public function llamarModalCreaReporteRuta()
    {
        $this->openModalCreaReporteRuta();
    }
  
    public function openModalReporte()
    {
        $this->isOpenReporte = true;
    }
    public function openModalReporteRuta() //Abre el Listado de Reportes a Control
    {
        $this->isOpenReporteRuta = true;
    }
    public function openModalCreaReporteRuta() //Abre el Form Crea de Reportes a Control
    {
        $this->isOpenCreaReporteRuta = true;
    }
  

    public function closeModalReporte()
    {
        $this->isOpenReporte = false;
    }
    public function closeModalReporteRuta()
    {
        $this->isOpenReporteRuta = false;
    }
    public function closeModalCreaReporteRuta()
    {
        $this->defaultReporte(); //Reseteamos los valores de las propiedades del Reporte
        $this->isOpenCreaReporteRuta = false;
    }

    public function creaControl()
    {
        //Validamos la Propiedades del componente
        $this->validate([
            'idhojavida' => 'required',
            'idcliente' => 'required',
            'fechadespacho' => 'required',
            'placatrailer' => 'required',
            'manfcarga' => 'required|unique:controlesrutas',
            'viaje' => 'required',
            'origen' => 'required',
            'destino' => 'required',
        ], $this->messages);

        $controlesss = new ControlesRutasModel;
        $controlesss->fechadespacho = $this->fechadespacho;
        $controlesss->idhojavida = $this->idhojavida;
        $controlesss->placatrailer = $this->placatrailer;
        $controlesss->viaje = $this->viaje;
        $controlesss->origen = $this->origen;
        $controlesss->destino = $this->destino;
        $controlesss->manfcarga = $this->manfcarga;
        $controlesss->idcliente = $this->idcliente;
        $controlesss->estado = 'ACTIVO';
        $controlesss->iduser = auth()->id(); // Guardamos el usuario que realiza la transaccion
        $controlesss->save();
        //Reseteamos los valores de las propiedades y cerramos el Modal
        $this->closemodal();
        session()->flash('message', "El Control Rutas, se Grabó Exitosamente.!!");
    }


    function default()
    {
        $this->resetErrorBag();
        $this->resetValidation();

        //Reseteamos los valores de las propiedades
        $this->reset([
            'idcontrolruta', 'fechadespacho', 'idhojavida', 'placatrailer',
            'viaje', 'origen', 'destino', 'manfcarga', 'iduser', 'fupdate',
            'iduser_update', 'estado', 'accion',
        ]);
    }

    function defaultReporte()
    {
        $this->resetErrorBag();
        $this->resetValidation();

        //Reseteamos los valores de las propiedades DEL Reporte GPS
        $this->reset([
            'idreporteruta', 'fechareporte', 'reporte', 'fotogps', 'observacion',
            'estadoreporte', 'iduser_reporte', 'fupdate_reporte',
            'iduser_update_reporte', 'accion2',
        ]);
    }



    public function confirmarItemEditarReporte(ReportesRutasModel $elReporteRuta, $elidR)
    {
        $this->llamarModalCreaReporteRuta(); //Llamamos el form modal con los datos del registro
        $this->idreporteruta = $elidR;
        $this->fechareporte = date('Y-m-d\TH:i', strtotime($elReporteRuta->fechareporte)); //Convertimos la Cadena de Fecha a Formato RFC 3339;
        $this->reporte = $elReporteRuta->reporte;
        $this->fotogps = $elReporteRuta->fotogps;
        $this->observacion = $elReporteRuta->observacion;
        $this->estadoreporte = $elReporteRuta->estado;
        $this->accion2 = 'actualiza';
    }


    public function ReporteRuta(ControlesRutasModel $elCOntrol, $laplaca)
    {
        $this->llamarModalReporteRuta(); //Llamamos el form modal con los datos del registro
        $this->idcontrolruta = $elCOntrol->idcontrolruta;
        $this->fechadespacho = date('Y-m-d\TH:i', strtotime($elCOntrol->fechadespacho)); //Convertimos la Cadena de Fecha a Formato RFC 3339;
        $this->idhojavida = $elCOntrol->idhojavida;
        $this->placatrailer = $elCOntrol->placatrailer;
        $this->viaje = $elCOntrol->viaje;
        $this->origen = $elCOntrol->origen;
        $this->destino = $elCOntrol->destino;
        $this->manfcarga = $elCOntrol->manfcarga;
        $this->estado = $elCOntrol->estado;
        $this->laplaca = $laplaca;


        $this->losreportes = ReportesRutasModel::where('idcontrolruta', $this->idcontrolruta)->orderBy('fechareporte', 'desc')->get();
    }

    //Metodos de Impresion
    public function imprimirreporte($desde, $hasta)
    {
        //return (new ControlPreExport($desde,$hasta))->download('Control Precintos ' . $desde . '-' . $hasta . '.pdf', \Maatwebsite\Excel\Excel::MPDF);
        return (new ControlRutasxClienteExport($desde, $hasta))->download('Control Rutas ' . $desde . '-' . $hasta . '.xlsx');
    }
}
