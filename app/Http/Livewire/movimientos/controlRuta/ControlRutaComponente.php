<?php

namespace App\Http\Livewire\movimientos\controlRuta;

use App\Models\RutasModel;
use App\Models\ReportesRutasModel;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Livewire\TemporaryUploadedFile;
use App\Exports\ControlRutasExport;



class ControlRutaComponente extends Component
{
    use WithFileUploads;
    use WithPagination;
    //Propiedades de Los campos de Contoles
    public  $ruta_id, $nombre, $origen, $destino, $conductor_id;

    public $accion = "crea";
    public $isOpen = 0; //para llamar a la vista con el Form Modal
    public $buscar; //Propiedad para busqueda


    protected $rules = [
        'conductor_id' => 'required',
    ];

    protected $messages = [
        'nombre.required' => 'Nombre Ruta, es necesario!!',
        'origen.required' => 'El Origen, es necesario!!',
        'destino.required' => 'El Destino es necesario. Verifique.!!',
        'conductor_id.required' => 'Dato es necesario. Verifique.!!',
    ];

    protected $queryStrings = [
        'buscar' => ['except' => ''],
    ];

    //Itdyaingenieria Software

    public function updatingBuscar()
    {
        $this->resetPage();
    }

    public function render()
    {
        //Encabezado de COntrol de Rutas
        $rutas = DB::table('rutas as r')
            ->leftjoin('conductores as c', 'r.conductor_id', 'c.conductor_id')
            ->select(
                'r.*',
                DB::raw('CONCAT(c.nombres, " ", c.apellidos) AS elconductor'),
            )
            ->where('r.nombre', 'LIKE', '%' . $this->buscar . '%')
            ->orwhere('c.nombres', 'LIKE', '%' . $this->buscar . '%')
            ->latest('r.nombre')
            ->paginate(10);

        $conductores = DB::table('conductores as c')
            ->select(
                'c.*',
            )
            ->get();

        return view(
            'livewire.movimientos.Rutas.controlR',
            [
                "rutas" => $rutas,
                "conductores" => $conductores,
            ]
        );
    }

    public function llamarModalCrear()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->openModal();
    }


    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->default(); //Reseteamos los valores de las propiedades
        $this->isOpen = false;
    }

    public function creaControl()
    {
        //Validamos la Propiedades del componente
        $this->validate([
            'nombre' => 'required',
            'origen' => 'required',
            'destino' => 'required',
            'conductor_id' => 'required',
        ], $this->messages);

        $rutas = new RutasModel;
        $rutas->nombre = $this->nombre;
        $rutas->origen = $this->origen;
        $rutas->destino = $this->destino;
        $rutas->conductor_id = $this->conductor_id;
        $rutas->save();
        //Reseteamos los valores de las propiedades y cerramos el Modal
        $this->closemodal();
        session()->flash('message', "Rutas, se Grabó Exitosamente.!!");
    }


    //Metodo de Actualizacion de Encabezado control
    public function actualizaControl()
    {
        $this->validate([
            'nombre' => 'required',
            'origen' => 'required',
            'destino' => 'required',
            'conductor_id' => 'required',
        ], $this->messages);

        $rutas = RutasModel::findOrFail($this->ruta_id);
        $rutas->nombre = $this->nombre;
        $rutas->origen = $this->origen;
        $rutas->destino = $this->destino;
        $rutas->conductor_id = $this->conductor_id;
        $rutas->Update();

        //Reseteamos los valores de las propiedades
        session()->flash('message', "El Registro: " . $this->ruta_id . ", se Actualizó Exitosamente.!!");
        $this->closemodal();
    }


    //Metodo de Actualizacion de Detalle de Reportes a rutas
    function default()
    {
        $this->resetErrorBag();
        $this->resetValidation();

        //Reseteamos los valores de las propiedades
        $this->reset([
            'ruta_id', 'nombre', 'origen', 'destino', 'conductor_id', 'accion',
        ]);
    }


    public function confirmarItemEditar(RutasModel $elCOntrol, $elid)
    {
        $this->llamarModalCrear(); //Llamamos el form modal con los datos del registro
        $this->ruta_id = $elid;
        $this->nombre = $elCOntrol->nombre;
        $this->origen = $elCOntrol->origen;
        $this->destino = $elCOntrol->destino;
        $this->conductor_id = $elCOntrol->conductor_id;
        $this->accion = 'actualiza';
    }




    public function eliminaControl($elControlLog)
    {
        $controlLog = RutasModel::findOrFail($elControlLog);
        $controlLog->delete();
        session()->flash('message', "El Registro: " . $elControlLog . ", se Eliminó, Exitosamente.!!");
    }

    //Listeners de escucha de eventos y ejecucion de acciones
    protected $listeners = [
        'deleteRow' => 'eliminaControl',
    ];
}
