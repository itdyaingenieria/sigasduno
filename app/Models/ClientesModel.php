<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientesModel extends Model
{
    use HasFactory;

    protected $table = 'clientes';
    protected $primaryKey = "cliente_id";
    public $timestamps = false;
    protected $fillable = [
        'numeroIdenficacion', 'nombres', 'apellidos', 'direccion', 'telefono', 'email', 'empresa_id',
    ];
}
