<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CodpersoModel extends Model
{
    use HasFactory;

    protected $table = 'conductores';
    protected $primaryKey = "conductor_id";
    public $timestamps = false;
    protected $fillable = [
        'nombres', 'apellidos', 'direccion', 'tipoLicencia', 'numeroLicencia', 'email', 'telefono', 'vehiculo_id'
    ];
}
