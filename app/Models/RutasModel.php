<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RutasModel extends Model
{
    use HasFactory;

    protected $table = 'rutas';
    protected $primaryKey = "ruta_id";
    public $timestamps = false;
    protected $fillable = [
        'nombre', 'origen', 'destino', 'conductor_id'
    ];
}
