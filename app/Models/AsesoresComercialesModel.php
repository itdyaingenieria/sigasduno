<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsesoresComercialesModel extends Model
{
    use HasFactory;

    protected $table = 'asesorcomercial';
    protected $primaryKey = "asesor_id";
    public $timestamps = false;
    protected $fillable = [
        'numeroIdentificacion', 'nombres', 'apellidos', 'antiguedad', 'tipo', 'fechaIngreso', 'direccion'
    ];
}
