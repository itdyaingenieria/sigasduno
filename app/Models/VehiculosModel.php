<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehiculosModel extends Model
{
    use HasFactory;

    protected $table = 'vehiculos';
    protected $primaryKey = "vehiculo_id";
    public $timestamps = false;
    protected $fillable = [
        'placa', 'marca', 'modelo', 'tipocombustible', 'empresa_id'
    ];
}
