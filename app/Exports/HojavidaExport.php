<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class HojavidaExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function __construct(string $id, string $lafoto,string $lafoto2, string $laplaca)
    {
        $this->id = $id;
        $this->lafoto = $lafoto;
        $this->lafoto2 = $lafoto2;
        $this->laplaca = $laplaca;
    }

    public function registerEvents(): array
    {
        return [

            BeforeExport::class => function (BeforeExport $event) {
                $event->getWriter()->getDelegate()
                    ->getProperties()
                    ->setCreator("Msc Ing Diego Fernando Yamá Andrade")
                    ->setLastModifiedBy("Itdyaingenieria - SITranslogex")
                    ->setTitle("Hoja de Vida N " . $this->id . "-" . $this->laplaca)
                    ->setSubject("Documento de SITranslogex")
                    ->setCompany("Translogex SAS")
                    ->setManager("Msc Ing Diego Fernando Yamá Andrade")
                    ->setDescription(
                        "Contiene Informacion Hoja Vida N" . $this->id . "-" . $this->laplaca . ", en sistema SITranslogex. Itdyaingenieria"
                    );
            },
            AfterSheet::class => function (AfterSheet $event) {
                // //Parametriamos la variable
                $lahoja = $event->sheet->getDelegate();

                //Configuramos el área de impresión
                $event->sheet->getPageSetup()->setPaperSize(1);
                $event->sheet->getPageSetup()->setPrintArea('A1:F63');


                $lahoja->setBreak('A46', \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::BREAK_ROW); //Saltamos la Pagina en PDF


                //COLOCAMOS EL LOGO EN EL REPORTE
                $lahoja->mergeCells('A1:B3');
                $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $objDrawing->setPath(public_path('img/' . 'logo1.jpg')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setWidthAndHeight(237, 80);
                $objDrawing->setOffsetX(3);
                $objDrawing->setOffsetY(3);
                $objDrawing->setWorksheet($event->sheet->getDelegate());


                //COLOCAMOS EL LOGO EN EL REPORTE PAGINA 2
                $lahoja->mergeCells('A47:B49');
                $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $objDrawing->setPath(public_path('img/' . 'logo1.jpg')); //your image path
                $objDrawing->setCoordinates('A47');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setWidthAndHeight(237, 80);
                $objDrawing->setOffsetX(3);
                $objDrawing->setOffsetY(3);
                $objDrawing->setWorksheet($event->sheet->getDelegate());

                //COLOCAMOS la imagen de conductor primera hoja en 4 filas
                $lahoja->mergeCells('E43:F46');
                $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $objDrawing->setPath(public_path($this->lafoto)); //your image path
                $objDrawing->setCoordinates('E43');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setWidthAndHeight(239, 107);
                $objDrawing->setOffsetX(3);
                $objDrawing->setOffsetY(3);
                $objDrawing->setWorksheet($event->sheet->getDelegate());

                //COLOCAMOS la imagen de Cabezote primera hoja en 4 filas
                $lahoja->mergeCells('A78:C87');
                $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $objDrawing->setPath(public_path($this->lafoto2)); //your image path
                $objDrawing->setCoordinates('A78');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setWidthAndHeight(259, 197);
                $objDrawing->setOffsetX(3);
                $objDrawing->setOffsetY(3);
                $objDrawing->setWorksheet($event->sheet->getDelegate());

                // //COLOCAMOS ANULADO EN LA ORDEN DE CARGUE
                // if ($this->estado == "ANULADO") {
                //     $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                //     $objDrawing->setPath(public_path('img/' . 'Anuladojd.png')); //your image path
                //     $objDrawing->setCoordinates('C21');
                //     $objDrawing->setResizeProportional(false);
                //     $objDrawing->setWidthAndHeight(387, 87);
                //     $objDrawing->setOffsetX(3);
                //     $objDrawing->setOffsetY(3);
                //     $objDrawing->setWorksheet($event->sheet->getDelegate());
                // }

                //PONEMOS LOS BORDES A TODAS LAS CELDAS DE LA VISTA
                $lahoja->getStyle('A1:F77')
                    ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);


                // //CENTRAMOS CONTENIDO VERTICAL Y HORIZONTALMENTE
                // $lahoja->getStyle('A22:B22')
                //     ->getAlignment()->applyFromArray(
                //         array('vertical' => 'center')
                //     )->setWrapText(true);
                // $lahoja->getStyle('C21')
                //     ->getAlignment()->applyFromArray(
                //         array('vertical' => 'center')
                //     )->setWrapText(true);

                // //COLOCAMOS COLOR CLARO DE INSTRUCCIONES EN LAS CELDAS
                // $lahoja->getStyle('C21')->getFont()->getColor()->setARGB('D5D5D7');
                // $lahoja->getStyle('A22:C22')->getFont()->getColor()->setARGB('D5D5D7');

                // //COLOCAMOS LA MARCA DEL SOFTWARE
                // $lahoja->mergeCells('A23:D23');
                // $lahoja->setCellValue('A23', 'software SIGTRANS .:. www.itdyaingenieria.pw');
            },
        ];
    }

    public function view(): View
    {
        //hacemos la consulta a la tabla y luego la pasamos a la vista que sera enviada a excel
        $mytime = Carbon::now('America/Bogota');
        $datosHojavida = DB::table('hojavidas as hv')
            ->leftJoin('vehiculos as v', 'hv.idvehiculo', 'v.idvehiculo')
            ->leftJoin('marcas as m', 'v.vehmarca', 'm.vehmarca')
            ->leftJoin('colores as col', 'v.vehcolor', 'col.vehcolor')
            ->leftJoin('codperso as conductor', 'hv.idperso_conductor', 'conductor.idperso')
            ->leftjoin('codperso as prop', 'v.vehidentpr', 'prop.idperso')
            ->leftjoin('codperso as ten', 'v.vehidenten', 'ten.idperso')
            ->leftjoin('codciud as cd', 'hv.idciudad', 'cd.idciudad')
            ->leftjoin('codciud as cdcond', 'conductor.idciudad', 'cdcond.idciudad')
            ->leftjoin('codciud as cdtenedor', 'ten.idciudad', 'cdtenedor.idciudad')
            ->leftjoin('codciud as cdpropietario', 'prop.idciudad', 'cdpropietario.idciudad')
            ->leftjoin('codciud as cdcondlicencia', 'conductor.idciudadlic', 'cdcondlicencia.idciudad')
            ->leftjoin('codciud as cdempresa1', 'conductor.empciudad1', 'cdempresa1.idciudad')
            ->leftjoin('codciud as cdempresa2', 'conductor.empciudad2', 'cdempresa2.idciudad')
            ->leftjoin('codciud as cdpersonal1', 'conductor.perciudad1', 'cdpersonal1.idciudad')
            ->leftjoin('codciud as cdpersonal2', 'conductor.perciudad2', 'cdpersonal2.idciudad')
            ->select(
                'hv.*',
                'v.vehplaca',
                'm.desc_marca',
                'col.desc_color',
                'v.vrhnmromot',
                'v.vehnroseri',
                'v.fvencisoat',
                'v.fvencitecnom',
                'conductor.fvencelic',
                'v.fvencisoat',
                'v.fvencitecnom',
                'v.nrotrailer',
                'v.marcatrailer',
                'v.vehmodelo',
                'v.satelital',
                'v.usuario',
                'v.clave',
                'ten.identperso as cedulatenedor',
                'ten.nmbrepers as nomtenedor',
                DB::raw('CONCAT(ten.priapellid," ", IFNULL(ten.seguapelli,"")) AS apellitenedor'),
                'cdtenedor.desc_ciud as ciudadtenedor',
                'ten.direpers as dirtenedor',
                'ten.telepers as teleperstenedor',
                'ten.telmovil as telmoviltenedor',
                'ten.banco as bancotenedor',
                'ten.tipocuenta as tipocuentatenedor',
                'ten.numcuenta as numcuentatenedor',
                'ten.ciudadcuenta as ciudadcuentatenedor',
                'ten.titularcuenta as titularcuentatenedor',
                'ten.ccnittitular as ccnittitulartenedor',
                'ten.email as emailtenedor',
                'prop.identperso as cedulapropietario',
                'prop.nmbrepers as nompropietario',
                DB::raw('CONCAT(prop.priapellid," ", IFNULL(prop.seguapelli,"")) AS apellipropietario'),
                'cdpropietario.desc_ciud as ciudadpropietario',
                'prop.direpers as dirpropietario',
                'prop.telepers as telepersprop',
                'prop.telmovil as telmovilprop',
                'prop.email as emailprop',
                'conductor.identperso as lacedulaconductor',
                'conductor.expedida as expedidaconductor',
                'conductor.nmbrepers as nomconductor',
                'conductor.direpers as dirconductor',
                'conductor.telepers as teleperscond',
                'conductor.telmovil as telmovilcond',
                'conductor.nmrolice as nmrolicecond',
                'conductor.licepers as categoriacond',
                'cdcondlicencia.desc_ciud as idciudadliccond',
                'conductor.fvencelic as fvenceliccond',
                'conductor.eps as epscond',
                'conductor.arl as arlcond',
                'conductor.banco as bancocond',
                'conductor.tipocuenta as tipocuentacond',
                'conductor.numcuenta as numcuentacond',
                'conductor.nombreconyuge',
                'conductor.apellidosconyuge',
                'conductor.telconyuge',
                'conductor.dirconyuge',
                'conductor.empnombre1',
                'cdempresa1.desc_ciud as ciudademp1',
                'conductor.emptelefono1',
                'conductor.empobservacion1',
                'conductor.empnombre2',
                'cdempresa2.desc_ciud as ciudademp2',
                'conductor.emptelefono2',
                'conductor.empobservacion2',
                'conductor.famnombre1',
                'conductor.famparentesco1',
                'conductor.famtelefono1',
                'conductor.famobservacion1',
                'conductor.famnombre2',
                'conductor.famparentesco2',
                'conductor.famtelefono2',
                'conductor.famobservacion2',
                'conductor.pernombre1',
                'cdpersonal1.desc_ciud as perciudad1',
                'conductor.pertelefono1',
                'conductor.perobservacion1',
                'conductor.pernombre2',
                'cdpersonal2.desc_ciud as perciudad2',
                'conductor.pertelefono2',
                'conductor.perobservacion2',
                DB::raw('CONCAT(conductor.priapellid," ", IFNULL(conductor.seguapelli,"")) AS apelliconductor'),
                'cdcond.desc_ciud as ciudadconductor',
                'cd.desc_ciud',
                DB::raw('CONCAT(conductor.nmbrepers, " ", conductor.priapellid," ", IFNULL(conductor.seguapelli,"")) AS elconductor'),
                DB::raw('CONCAT(prop.nmbrepers, " ", prop.priapellid," ", IFNULL(prop.seguapelli,"")) AS propietario'),
                DB::raw('CONCAT(ten.nmbrepers, " ", ten.priapellid," ", IFNULL(ten.seguapelli,"")) AS tenedor')
            )
            ->where('hv.idhojavida', '=', $this->id)
            ->first();


        //dd($datosHojavida);

        return view('livewire.movimientos.hojavida.hojavida_excel', [
            "mytime" => $mytime,
            "datosHojavida" => $datosHojavida,

        ]);
    }
}
