<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class ControlRutasExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable;
    public function __construct(string $desde, string $hasta)
    {
        $this->desde = $desde;
        $this->hasta = $hasta;
    }

    public function registerEvents(): array
    {
        return [

            BeforeExport::class => function (BeforeExport $event) {
                $event->getWriter()->getDelegate()
                    ->getProperties()
                    ->setCreator("Msc Ing Diego Fernando Yamá Andrade")
                    ->setLastModifiedBy("Itdyaingenieria - SITranslogex")
                    ->setTitle("Reporte Control Rutas")
                    ->setSubject("Documento de SITranslogex")
                    ->setCompany("Translogex SAS")
                    ->setManager("Msc Ing Diego Fernando Yamá Andrade")
                    ->setDescription(
                        "Contiene Informacion Control Rutas y Reportes, en sistema SITranslogex. Itdyaingenieria"
                    );
            },
            AfterSheet::class => function (AfterSheet $event) {
                // //Parametriamos la variable
                $lahoja = $event->sheet->getDelegate();

                //Configuramos el área de impresión
                //$event->sheet->getPageSetup()->setPaperSize(1);
                //$event->sheet->getPageSetup()->setPrintArea('A1:F23');

                //COLOCAMOS EL LOGO EN EL REPORTE
                $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $objDrawing->setPath(public_path('img/' . 'logo1.jpg')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setWidthAndHeight(237, 80);
                $objDrawing->setOffsetX(3);
                $objDrawing->setOffsetY(3);
                $objDrawing->setWorksheet($event->sheet->getDelegate());

                //PONEMOS LOS BORDES A TODAS LAS CELDAS DE LA VISTA
                $lahoja->getStyle('A1:N3')
                    ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $highestRow = $lahoja->getHighestRow();

                $lahoja->getColumnDimension('F')->setAutoSize(false);
                $lahoja->getColumnDimension('F')->setWidth(17.72);
                $lahoja->getColumnDimension('H')->setAutoSize(false);
                $lahoja->getColumnDimension('H')->setWidth(17.72);
                $lahoja->getProtection()->setSheet(true);    // Needs to be set to true in order to enable any worksheet protection!

                for ($row = 10; $row <= $highestRow; $row++) {
                    // $lahoja->getStyle("H$row")->getAlignment()->setWrapText(true);
                    $lahoja->getStyle('A10:N' . $row)
                        ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $lahoja->getStyle('A10:N' . $row)
                        ->getAlignment()->applyFromArray(
                            array('vertical' => 'center')
                        )->setWrapText(true);
                    // $lahoja->mergeCells("A10:A" . $row);

                    $lahoja->protectCells('A10:I' . $row, '12062021'); //Protegemos la edicion con Contrasena
                }


                $highestColumn = $lahoja->getHighestColumn(); // e.g 'F'
                // Increment the highest column letter
                $highestColumn++;

                $job_num_chk = '';
                $groupings = [];

                //format the inserted records
                for ($row = 11; $row <= $highestRow; ++$row) {
                    for ($col = 'A'; $col != $highestColumn; ++$col) {
                        $lahoja->getStyle($col . $row)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                        $lahoja->getStyle($col . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        if ($col == 'A') {
                            $cur_job_num = $lahoja->getCell($col . $row)->getValue(); //get current job number
                            //check if job number is different
                            if ($job_num_chk !== $cur_job_num) {
                                $job_num_chk = $cur_job_num; //update job number
                            }
                            $row_m = $row - 1;
                            $row_a = $row + 1;
                            $above = $lahoja->getCell($col . $row_m)->getValue();
                            $below = $lahoja->getCell($col . $row_a)->getValue();

                            if ($cur_job_num !== $above) {
                                $groupings[$cur_job_num]['start'] = $row;
                            } elseif ($cur_job_num !== $below) {
                                $groupings[$cur_job_num]['end'] = $row;
                            }
                        }
                    }
                }
                // dd($groupings);
                //setup row groupings
                foreach ($groupings as $g) {
                    $end = isset($g['end']) ? $g['end'] : $g['start'];
                    $lahoja->mergeCells("A" . $g['start'] . ":A" . $end);
                    $lahoja->mergeCells("B" . $g['start'] . ":B" . $end);
                    $lahoja->mergeCells("C" . $g['start'] . ":C" . $end);
                    $lahoja->mergeCells("D" . $g['start'] . ":D" . $end);
                    $lahoja->mergeCells("E" . $g['start'] . ":E" . $end);
                    $lahoja->mergeCells("F" . $g['start'] . ":F" . $end);
                    $lahoja->mergeCells("G" . $g['start'] . ":G" . $end);
                    $lahoja->mergeCells("H" . $g['start'] . ":H" . $end);
                    $lahoja->mergeCells("I" . $g['start'] . ":I" . $end);
                    $lahoja->mergeCells("J" . $g['start'] . ":J" . $end);
                    $lahoja->mergeCells("K" . $g['start'] . ":K" . $end);
                }

                //COLOCAMOS LA MARCA DEL SOFTWARE
                $lahoja->setCellValue('L7', 'software SITranslogex .:. www.itdyaingenieria.com');
                $lahoja->getStyle('L7')->getFont()->setSize(8)->setBold(false);
                $lahoja->getStyle('A10:M10')->getFont()->setBold(true);
            },
        ];
    }

    public function view(): View
    {
        //hacemos la consulta a la tabla y luego la pasamos a la vista que sera enviada a excel
        $mytime = Carbon::now('America/Bogota');

        $JuanDiego = DB::table('controlesrutas as c')
            ->leftjoin('hojavidas as hv', 'c.idhojavida', 'hv.idhojavida')
            ->leftJoin('vehiculos as v', 'hv.idvehiculo', 'v.idvehiculo')
            ->leftJoin('codperso as conductor', 'hv.idperso_conductor', 'conductor.idperso')
            ->leftJoin('reportesrutas as r', 'c.idcontrolruta', 'r.idcontrolruta')
            ->leftJoin('clientes as cl', 'c.idcliente', 'cl.idcliente')
            ->select(
                'c.*',
                'v.vehplaca',
                'v.satelital',
                'v.usuario',
                'v.clave',
                'hv.idvehiculo',
                'conductor.fvencelic',
                'cl.clinmbre',
                DB::raw('CONCAT(conductor.nmbrepers, " ", conductor.priapellid," ", IFNULL(conductor.seguapelli,"")) AS elconductor'),
                'r.*',
            )
            ->where('c.fechadespacho', '>=', $this->desde)
            ->where('c.fechadespacho', '<=', $this->hasta)
            ->get();

        //dd($JuanDiego);
        return view('livewire.movimientos.controlRutas.controlRuta_excel', [
            "mytime" => $mytime,
            "JuanDiego" => $JuanDiego,
            "desde" => $this->desde,
            "hasta" => $this->hasta,
        ]);
    }
}
