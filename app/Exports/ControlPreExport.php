<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class ControlPreExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable;
    public function __construct(string $desde, string $hasta)
    {
        $this->desde = $desde;
        $this->hasta = $hasta;
    }

    public function registerEvents(): array
    {
        return [

            BeforeExport::class => function (BeforeExport $event) {
                $event->getWriter()->getDelegate()
                    ->getProperties()
                    ->setCreator("Msc Ing Diego Fernando Yamá Andrade")
                    ->setLastModifiedBy("Itdyaingenieria - SITranslogex")
                    ->setTitle("Reporte Control Precintos")
                    ->setSubject("Documento de SITranslogex")
                    ->setCompany("Translogex SAS")
                    ->setManager("Msc Ing Diego Fernando Yamá Andrade")
                    ->setDescription(
                        "Contiene Informacion Control Precintos, en sistema SITranslogex. Itdyaingenieria"
                    );
            },
            AfterSheet::class => function (AfterSheet $event) {
                // //Parametriamos la variable
                $lahoja = $event->sheet->getDelegate();

                //Configuramos el área de impresión
                //$event->sheet->getPageSetup()->setPaperSize(1);
                //$event->sheet->getPageSetup()->setPrintArea('A1:F23');

                //COLOCAMOS EL LOGO EN EL REPORTE
                $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $objDrawing->setPath(public_path('img/' . 'logo1.jpg')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setWidthAndHeight(237, 80);
                $objDrawing->setOffsetX(3);
                $objDrawing->setOffsetY(3);
                $objDrawing->setWorksheet($event->sheet->getDelegate());

                //PONEMOS LOS BORDES A TODAS LAS CELDAS DE LA VISTA
                $lahoja->getStyle('A1:I3')
                    ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $highestRow = $lahoja->getHighestRow();

                $lahoja->getColumnDimension('F')->setAutoSize(false);
                $lahoja->getColumnDimension('F')->setWidth(27.72);
                $lahoja->getColumnDimension('H')->setAutoSize(false);
                $lahoja->getColumnDimension('H')->setWidth(77.72);
                $lahoja->getProtection()->setSheet(true);    // Needs to be set to true in order to enable any worksheet protection!

                for ($row = 10; $row <= $highestRow; $row++) {
                    // $lahoja->getStyle("H$row")->getAlignment()->setWrapText(true);
                    $lahoja->getStyle('A10:I' . $row)
                        ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $lahoja->getStyle('A10:I' . $row)
                        ->getAlignment()->applyFromArray(
                            array('vertical' => 'center')
                        )->setWrapText(true);
                    
                    $lahoja->protectCells('A10:I'.$row, '12062021');//Protegemos la edicion con Contrasena
                }

                //COLOCAMOS LA MARCA DEL SOFTWARE
                $lahoja->setCellValue('H7', 'software SITranslogex .:. www.itdyaingenieria.com');
                $lahoja->getStyle('H7')->getFont()->setSize(8)->setBold(false);
                $lahoja->getStyle('A10:I10')->getFont()->setBold(true);
            },
        ];
    }

    public function view(): View
    {
        //hacemos la consulta a la tabla y luego la pasamos a la vista que sera enviada a excel
        $mytime = Carbon::now('America/Bogota');
        $JuanDiego = DB::table('controlesprecintos as c')
            ->leftjoin('hojavidas as hv', 'c.idhojavida', 'hv.idhojavida')
            ->leftJoin('vehiculos as v', 'hv.idvehiculo', 'v.idvehiculo')
            ->leftJoin('codperso as conductor', 'hv.idperso_conductor', 'conductor.idperso')
            ->select(
                'c.*',
                'v.vehplaca',
                'conductor.fvencelic',
                DB::raw('CONCAT(conductor.nmbrepers, " ", conductor.priapellid," ", IFNULL(conductor.seguapelli,"")) AS elconductor'),
            )
            ->where('c.fechacontrolpre', '>=', $this->desde)
            ->where('c.fechacontrolpre', '<=', $this->hasta)
            ->get();
        //dd($JuanDiego);
        return view('livewire.movimientos.controlPrecintos.controlPre_excel', [
            "mytime" => $mytime,
            "JuanDiego" => $JuanDiego,
            "desde" => $this->desde,
            "hasta" => $this->hasta,
        ]);
    }
}
