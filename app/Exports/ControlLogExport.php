<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class ControlLogExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable;
    public function __construct(string $id, string $lafirma, string $laplaca)
    {
        $this->id = $id;
        $this->lafirma = $lafirma;
        $this->laplaca = $laplaca;
    }

    public function registerEvents(): array
    {
        return [

            BeforeExport::class => function (BeforeExport $event) {
                $event->getWriter()->getDelegate()
                    ->getProperties()
                    ->setCreator("Msc Ing Diego Fernando Yamá Andrade")
                    ->setLastModifiedBy("Itdyaingenieria - SITranslogex")
                    ->setTitle("Control Logístico N " . $this->id . "-" . $this->laplaca)
                    ->setSubject("Documento de SITranslogex")
                    ->setCompany("Translogex SAS")
                    ->setManager("Msc Ing Diego Fernando Yamá Andrade")
                    ->setDescription(
                        "Contiene Informacion Control Logístico N" . $this->id . "-" . $this->laplaca . ", en sistema SITranslogex. Itdyaingenieria"
                    );
            },
            AfterSheet::class => function (AfterSheet $event) {
                // //Parametriamos la variable
                $lahoja = $event->sheet->getDelegate();

                //Configuramos el área de impresión
                $event->sheet->getPageSetup()->setPaperSize(1);
                $event->sheet->getPageSetup()->setPrintArea('A1:F23');



                //COLOCAMOS EL LOGO EN EL REPORTE
                $lahoja->mergeCells('A1:A3');
                $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $objDrawing->setPath(public_path('img/' . 'logo1.jpg')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setWidthAndHeight(237, 80);
                $objDrawing->setOffsetX(3);
                $objDrawing->setOffsetY(3);
                $objDrawing->setWorksheet($event->sheet->getDelegate());

                //dd(!$this->lafirma);
                //COLOCAMOS la firma del Conductor
                if ($this->lafirma) {
                    $lahoja->mergeCells('A20:C22');
                    $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $objDrawing->setPath(public_path($this->lafirma)); //your image path
                    $objDrawing->setCoordinates('A20');
                    $objDrawing->setResizeProportional(false);
                    $objDrawing->setWidthAndHeight(249, 107);
                    $objDrawing->setOffsetX(1);
                    $objDrawing->setOffsetY(3);
                    $objDrawing->setWorksheet($event->sheet->getDelegate());
                }
                // //COLOCAMOS ANULADO EN LA ORDEN DE CARGUE
                // if ($this->estado == "ANULADO") {
                //     $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                //     $objDrawing->setPath(public_path('img/' . 'Anuladojd.png')); //your image path
                //     $objDrawing->setCoordinates('C21');
                //     $objDrawing->setResizeProportional(false);
                //     $objDrawing->setWidthAndHeight(387, 87);
                //     $objDrawing->setOffsetX(3);
                //     $objDrawing->setOffsetY(3);
                //     $objDrawing->setWorksheet($event->sheet->getDelegate());
                // }

                //PONEMOS LOS BORDES A TODAS LAS CELDAS DE LA VISTA
                $lahoja->getStyle('A1:F23')
                    ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                //CENTRAMOS CONTENIDO VERTICAL Y HORIZONTALMENTE
                $lahoja->getStyle('A8:C18')
                    ->getAlignment()->applyFromArray(
                        array('vertical' => 'center')
                    )->setWrapText(true);
                $lahoja->getStyle('D8:F15')
                    ->getAlignment()->applyFromArray(
                        array('vertical' => 'center')
                    )->setWrapText(true);
                $lahoja->getStyle('A19:F19')
                    ->getAlignment()->applyFromArray(
                        array('vertical' => 'center')
                    )->setWrapText(true);

                // //COLOCAMOS COLOR CLARO DE INSTRUCCIONES EN LAS CELDAS
                // $lahoja->getStyle('C21')->getFont()->getColor()->setARGB('D5D5D7');
                // $lahoja->getStyle('A22:C22')->getFont()->getColor()->setARGB('D5D5D7');

                //COLOCAMOS LA MARCA DEL SOFTWARE
                $lahoja->mergeCells('A24:D24');
                $lahoja->setCellValue('A24', 'software SITranslogex .:. www.itdyaingenieria.com');
                $lahoja->getStyle('A24')->getFont()->setSize(8)->setBold(false);
            },
        ];
    }

    public function view(): View
    {
        //hacemos la consulta a la tabla y luego la pasamos a la vista que sera enviada a excel
        $mytime = Carbon::now('America/Bogota');
        $ariannaAbigail = DB::table('controles as c')
            ->leftjoin('hojavidas as hv', 'c.idhojavida', 'hv.idhojavida')
            ->leftJoin('vehiculos as v', 'hv.idvehiculo', 'v.idvehiculo')
            ->leftJoin('codperso as conductor', 'hv.idperso_conductor', 'conductor.idperso')
            ->select(
                'c.*',
                'v.vehplaca',
                'conductor.fvencelic',
                DB::raw('CONCAT(conductor.nmbrepers, " ", conductor.priapellid," ", IFNULL(conductor.seguapelli,"")) AS elconductor'),
            )
            ->where('c.idcontrol', '=', $this->id)
            ->first();
        //dd($ariannaAbigail);
        return view('livewire.movimientos.controlLogistico.controlLog_excel', [
            "mytime" => $mytime,
            "ariannaAbigail" => $ariannaAbigail,
        ]);
    }
}
