# Acerca de SISTEMA .:.
Estamos enfocados en crear herramientas y soluciones a la medida, empresas medianas y pequeñas, tenemos un sueño, queremos llegar a sistematizar la mayoria de la empresas de Narino.

# SIGASDUNO 2021 - ADSI

- Ultima version de SIGASDUNO ES:

SIGASDUNO 2021 es un sistema de informacion que se diseña para la empresa GASDUNO de Colombia. posee varios modulos, bajo la filosofia de desarrollo a la medida, por parte de itdyaingenieria

## Documentacion Oficial

La documentacion oficial del sistema se encuentra en  [Itdyaingenieria website](http://www.itdyaingenieria.pw).

## Instalacion

### Requisitos de instalación

En la siguiente tabla se muestran las tecnologías que usa el aplicativo. Para poder hacer un despliegue sin problemas se recomienda contar con las versiones específicas para PHP y Mysql.

* Tecnología
- Versión PHP 7.2 o superior, se recomienda 8.0
- Mysql 5.7 o 8.0
- Laravel 8 o superior
- Livewire
- Javascript
- Alpine.js
- Tener instalado Laragon

### Configuración del proyecto
En la carpeta www de laragon encontrará todo sus proyectos, puede ubicarla en el directorio que seleccionó al momento de la instalación o siguiendo los pasos de la imágen.

En este caso ahi se realizará la instalación del proyecto, use la terminal de laragon y ubíquese en la carpeta “c:\laragon\www”, la ubicación cambiará dependiendo de donde haya instalado el programa.

Cuando se encuentre en dicha ruta, ejecute el siguiente comando:
- git clone https://gitlab.com/itdyaingenieria/sigasduno.git

Le solicitará su usuario y password de gitlab, debe escribirlos para que puede continuar el proceso.

Cuando termine de descargar el proyecto, ingrese a la carpeta del proyecto, use el siguiente comando en la terminal.
cd sigasduno


Ubicado en la carpeta del proyecto, instale las dependencias, ejecutando el siguiente comando.
composer install

Consecuentemente debe crear el archivo .env dentro de la carpeta del proyecto, tome como referencia las variables que se encuentran en el archivo .env.example.

Ahora debe crear la base de datos principal, en este caso se llamará sigasduno, y así debe estar configurada en el archivo .env.

Después de ello, diríjase a la carpeta de su proyecto usando la terminal, y ejecute los siguientes comandos.
- php artisan key:generate
- composer dump-autoload
- php artisan storage:link

## Security Vulnerabilities

If you discover a security vulnerability within LARAVEL, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

La Licencia de SIGASDUNO, es desarrollo a la medida para la empresa GASDUNO [MIT license](http://www.gasduno.com).
## Academico SENA