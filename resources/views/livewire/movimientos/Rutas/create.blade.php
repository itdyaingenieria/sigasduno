<div class="fixed inset-0 z-10 overflow-y-auto duration-75 ease-out ">
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <!-- Modal diseñado por Diego Fernando Yamá Andrade. -->
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>​
        <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white shadow-xl rounded-xl sm:my-8 sm:align-middle sm:max-w-7xl sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            {{-- Encabezado de Mi modal dfya --}}
            <div
                class="justify-between px-4 py-3 font-bold text-indigo-900 bg-indigo-900 sm:px-4 sm:flex sm:flex-row-reverse">
                @if ($accion == 'crea')
                    <span
                        class="flex w-full px-4 py-1 mb-1 text-indigo-900 rounded-md shadow-sm  mibg3 sm:ml-3 sm:w-auto">
                        <strong>Nuevo Control Rutas .:. SIGASDUNO</strong>
                    </span>
                @else
                    <span
                        class="flex w-full px-4 py-1 mb-1 text-indigo-900 rounded-md shadow-sm  mibg3 sm:ml-3 sm:w-auto">
                        <strong>Editar Control Rutas .:. SIGASDUNO</strong>
                    </span>
                @endif
            </div>
            <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4 ">
                <div class="grid grid-cols-4 gap-1 mb-1 md:grid-cols-4">
                    <div class="col-span-4 sm:col-span-2">
                        <label for="lbl_nombre" class="block text-sm font-bold text-gray-700 ">
                            Nombre Ruta</label>
                        <input type="text" class="form-juandiego" wire:model="nombre">
                        @error('nombre')
                            <p class="text-xs italic text-red-600">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="col-span-4 mb-1 sm:col-span-2">
                        <label for="lbl_origen" class="block text-sm font-bold text-gray-700 ">
                            Origen</label>
                        <input type="text" class="form-juandiego" wire:model="origen">
                        @error('origen')
                            <p class="text-xs italic text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-span-4 mb-1 sm:col-span-2">
                        <label for="lbl_destino" class="block text-sm font-bold text-gray-700 ">
                            Destino</label>
                        <input type="text" class="form-juandiego" wire:model="destino">
                        @error('destino')
                            <p class="text-xs italic text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
     
                    <div class="col-span-4 mb-1 sm:col-span-2">
                        <label for="lbl_conductor_id" class="block text-sm font-bold text-gray-700 ">Conductor</label>
                        <select wire:model="conductor_id" class="text-sm form-juandiego">
                            <option value="" selected>Escoja su opcion....</option>
                            @foreach ($conductores as $conductor)
                                <option value={{ $conductor->conductor_id }}>
                                    {{ $conductor->numeroLicencia }}-{{ $conductor->nombres }}-{{ $conductor->apellidos }}
                                </option>
                            @endforeach
                        </select>
                        @error('conductor_id')
                            <p class="text-xs italic text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
            </div>


            {{-- Pie del Modal footer --}}
            <div class="px-4 py-3 bg-gray-50 sm:px-6 sm:flex sm:flex-row-reverse">
                @if ($accion == 'crea')
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button wire:click.prevent="creaControl()" type="button"
                            class="justify-center w-full form-botoncrear">
                            Grabar
                            <i wire:loading wire:target="creaControl" class="ml-2 fa fa-refresh fa-spin"></i>
                        </button>
                    </span>
                @else
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        @can('actualizar_ruta')
                            <button wire:click.prevent="actualizaControl()" type="button"
                                class="justify-center w-full form-botoncrear">
                                Actualizar
                                <i wire:loading wire:target="actualizaControl" class="ml-2 fa fa-refresh fa-spin"></i>
                            </button>
                        @endcan
                    </span>

                @endif
                <span class="flex w-full mt-3 rounded-md shadow-sm sm:mt-0 sm:w-auto">
                    <button wire:click="closeModal()" type="button" class="justify-center w-full form-botoneliminar">
                        Cancelar
                        <i wire:loading wire:target="closeModal" class="ml-2 fa fa-spinner fa-spin"></i>
                    </button>
                </span>
            </div>

        </div>
    </div>
</div>
</div>

<script type="text/javascript">

</script>
