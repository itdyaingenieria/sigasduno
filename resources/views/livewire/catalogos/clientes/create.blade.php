<div class="fixed inset-0 z-10 overflow-y-auto duration-75 ease-out ">
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <!-- Modal diseñado por Msc Ing Diego Fernando Yamá Andrade. -->
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>​
        <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white shadow-xl rounded-xl sm:my-8 sm:align-middle sm:max-w-5xl sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            {{-- Encabezado de Mi modal SIGASDUNO --}}
            <div
                class="justify-between px-4 py-3 font-bold text-indigo-900 bg-indigo-900 sm:px-4 sm:flex sm:flex-row-reverse">
                @if ($accion == 'crea')
                    <span
                        class="flex w-full px-4 py-1 mb-1 text-indigo-900 rounded-md shadow-sm mibg3 sm:ml-3 sm:w-auto">
                        <strong>Nuevo Cliente .:. SIGASDUNO</strong>
                    </span>
                @else
                    <span
                        class="flex w-full px-4 py-1 mb-1 text-indigo-900 rounded-md shadow-sm mibg3 sm:ml-3 sm:w-auto">
                        <strong>Editar Cliente .:. SIGASDUNO</strong>
                    </span>
                @endif
            </div>
            <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4 ">
                <div class="grid grid-cols-4 gap-1 mb-1 md:grid-cols-4">
                    <div class="col-span-4 mb-1 sm:col-span-1">
                        <label for="lbl_clinid" class="block mb-1 text-sm font-bold text-gray-700"># IDENTIFICACION
                        </label>
                        <input type="text" class="font-bold form-juandiego" placeholder="# Identificacion del Cliente."
                            wire:model="numeroIdenficacion">
                        @error('numeroIdenficacion')
                            <p class="text-xs italic text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-span-4 mb-1 sm:col-span-2">
                        <label for="lbl_clinmbre" class="block mb-1 text-sm font-bold text-gray-700">Nombres Cliente
                        </label>
                        <input type="text" class="font-bold form-juandiego bg-blue-50" placeholder="Nombre del Cliente."
                            wire:model="nombres">
                        @error('nombres')
                            <p class="text-xs italic text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-span-4 mb-1 sm:col-span-1">
                        <label for="lbl_clinmbre" class="block mb-1 text-sm font-bold text-gray-700">Apellidos Cliente
                        </label>
                        <input type="text" class="font-bold form-juandiego bg-blue-50" placeholder="Apellidos del Cliente."
                            wire:model="apellidos">
                        @error('apellidos')
                            <p class="text-xs italic text-red-600">{{ $message }}</p>
                        @enderror
                    </div>


                    <div class="col-span-4 mb-1 sm:col-span-2">
                        <label for="lbl_clidrccion" class="block mb-1 text-sm font-bold text-gray-700">Direccion Cliente
                        </label>
                        <input type="text" class="font-bold form-juandiego " placeholder="Direccion del Cliente."
                            wire:model="direccion">
                        @error('direccion')
                            <p class="text-xs italic text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-span-4 mb-1 sm:col-span-1">
                        <label for="lbl_clitlfno" class="block mb-1 text-sm font-bold text-gray-700">Teléfono Cliente
                        </label>
                        <input type="text" class="font-bold form-juandiego" placeholder="Teléfono del Cliente."
                            wire:model="telefono">
                        @error('telefono')
                            <p class="text-xs italic text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-span-4 mb-1 sm:col-span-1">
                        <label for="lbl_cliemail" class="block mb-1 text-sm font-bold text-gray-700">Email Cliente
                        </label>
                        <input type="text" class="font-bold form-juandiego" placeholder="Email del Cliente."
                            wire:model="email">
                        @error('email')
                            <p class="text-xs italic text-red-600">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
            </div>


            {{-- Pie del Modal footer --}}
            <div class="px-4 py-3 bg-gray-50 sm:px-6 sm:flex sm:flex-row-reverse">
                @if ($accion == 'crea')
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button wire:click.prevent="creaCliente()" type="button"
                            class="justify-center w-full form-botoncrear">
                            Grabar
                            <i wire:loading wire:target="creaCliente" class="ml-2 fa fa-refresh fa-spin"></i>
                        </button>
                    </span>
                @else
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        @can('actualizar_cliente')
                            <button wire:click.prevent="actualizaCliente()" type="button"
                                class="justify-center w-full form-botoncrear">
                                Actualizar
                                <i wire:loading wire:target="actualizaCliente" class="ml-2 fa fa-refresh fa-spin"></i>
                            </button>
                        @endcan
                    </span>

                @endif
                <span class="flex w-full mt-3 rounded-md shadow-sm sm:mt-0 sm:w-auto">
                    <button wire:click="closeModal()" type="button" class="justify-center w-full form-botoneliminar">
                        Cancelar
                        <i wire:loading wire:target="closeModal" class="ml-2 fa fa-spinner fa-spin"></i>
                    </button>
                </span>
            </div>

        </div>
    </div>
</div>
