<div x-data="{
    openTab: 1,
    activeClasses: 'border-l border-t border-r rounded-t-xl text-indigo-900',
    inactiveClasses: 'rounded-t-sm text-blue-400 hover:text-indigo-800'}" class="p-1">//Utilizamos Alpine Js
    <div class="fixed inset-0 z-10 overflow-y-auto duration-75 ease-out ">
        <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 transition">
                <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
            <!-- Modal diseñado por Msc Ing Diego Fernando Yamá Andrade. -->
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>​
            <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white shadow-xl rounded-xl sm:my-8 sm:align-middle sm:max-w-5xl sm:w-full"
                role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                {{-- Encabezado de Mi modal ItdyaingenieriA --}}
                <div
                    class="justify-between px-4 py-3 font-bold text-indigo-900 bg-indigo-900 sm:px-4 sm:flex sm:flex-row-reverse">
                    @if ($accion == 'crea')
                        <span
                            class="flex w-full px-4 py-1 mb-1 text-indigo-900 rounded-md shadow-sm mibg3 sm:ml-3 sm:w-auto">
                            <strong>Nuevo Condcutor .:. Sigasduno</strong>
                        </span>
                    @else
                        <span
                            class="flex w-full px-4 py-1 mb-1 text-indigo-900 rounded-md shadow-sm mibg3 sm:ml-3 sm:w-auto">
                            <strong>Editar Condcutor .:. Sigasduno</strong>
                        </span>
                    @endif
                    <ul class="flex mt-0 border-b">
                        <li @click="openTab = 1" :class="{ '-mb-px': openTab === 1 }" class="mr-1 -mb-px">
                            <a :class="openTab === 1 ? activeClasses : inactiveClasses"
                                class="inline-block px-4 py-1 font-semibold mibg3 " href="#">
                                Generales
                            </a>
                        </li>
                        <li @click="openTab = 2" :class="{ '-mb-px': openTab === 2 }" class="mr-1">
                            <a :class="openTab === 2 ? activeClasses : inactiveClasses"
                                class="inline-block px-4 py-1 font-semibold mibg3 " href="#">Referencias</a>
                        </li>
                    </ul>
                </div>

                <div x-show="openTab === 1">
                    <div class="px-4 pt-5 pb-2 bg-white sm:p-2 sm:pb-2 ">
                        <div class="grid grid-cols-4 gap-1 mb-1 md:grid-cols-4">

                            <div class="col-span-4 mb-1 sm:col-span-2">
                                <label for="lbl_nmbrepers" class="block mb-1 text-sm font-bold text-gray-700">Nombres
                                    Completos</label>
                                <input type="text" class="form-juandiego" id="nmbrepers" placeholder="Nombres Completos"
                                    wire:model="nombres">
                                @error('nombres')
                                    <p class="text-xs italic text-red-600">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-span-4 mb-1 sm:col-span-2">
                                <label for="lbl_priapellid"
                                    class="block mb-1 text-sm font-bold text-gray-700">Apellidos</label>
                                <input type="text" class="form-juandiego" id="priapellid" placeholder="Apellidos"
                                    wire:model="apellidos">
                                @error('apellidos')
                                    <p class="text-xs italic text-red-600">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-span-4 mb-1 sm:col-span-2">
                                <label for="lbl_direpers"
                                    class="block text-sm font-medium text-gray-700">Dirección</label>
                                <input wire:model='direccion' type="text" name="direccion" id="direccion"
                                    class="form-juandiego">
                                @error('direccion')
                                    <p class="text-xs italic text-red-600">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-span-4 mb-1 sm:col-span-2">
                                <label for="lbl_tipoLicencia" class="block text-sm font-medium text-gray-700">Tipo
                                    Licencia</label>
                                <input maxlength="2" type="text" wire:model="tipoLicencia" class="form-juandiego">
                                @error('tipoLicencia')
                                    <p class="text-xs italic text-red-600">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-span-4 mb-1 sm:col-span-2">
                                <label for="lbl_numeroLicencia" class="block text-sm font-medium text-gray-700">Número
                                    Licencia</label>
                                <input maxlength="13" type="text" wire:model="numeroLicencia" class="form-juandiego">
                                @error('numeroLicencia')
                                    <p class="text-xs italic text-red-600">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-span-4 mb-1 sm:col-span-2">
                                <label for="lbl_email" class="block text-sm font-medium text-gray-700">Email</label>
                                <input maxlength="53" type="email" wire:model="email" class="form-juandiego">
                                @error('email')
                                    <p class="text-xs italic text-red-600">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-span-4 mb-1 sm:col-span-2">
                                <label for="lbl_telefonoe"
                                    class="block text-sm font-medium text-gray-700">Telefono</label>
                                <input maxlength="13" type="text" wire:model="telefono" class="form-juandiego">
                            </div>
                            <div class="col-span-4 mb-1 sm:col-span-2">
                                <label for="lbl_vehiculo_id" class="block text-sm font-bold text-gray-700 ">Vehiculo
                                    Asignar</label>
                                <select wire:model="vehiculo_id" class="text-sm form-juandiego">
                                    <option value="" selected>Escoja su opcion....</option>
                                    @foreach ($vehiculos as $vehiculo)
                                        <option value={{ $vehiculo->vehiculo_id }}>{{ $vehiculo->placa }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('vehiculo_id')
                                    <p class="text-xs italic text-red-600">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div x-show="openTab === 2">
                    <div class="px-4 pt-1 pb-1 ml-2 mr-2 bg-white sm:p-1 sm:pb-1 ">
                        <h4 class="px-4 font-bold bg-indigo-900 rounded-lg text-indigo-50">Datos Aumentar Informacion
                            .:.
                        </h4>
                        <div class="grid grid-cols-4 gap-1 mb-1 md:grid-cols-4">
                            <div class="col-span-4 mb-1 sm:col-span-1">
                                <label for="lbl_nombreconyuge" class="block text-sm font-bold text-gray-700">Nombres
                                </label>
                                <input type="text" class="form-juandiego" placeholder="Nombres Completos"
                                    wire:model="nombreconyuge">
                            </div>
                            <div class="col-span-4 mb-1 sm:col-span-1">
                                <label for="lbl_apellidosconyuge"
                                    class="block text-sm font-bold text-gray-700">Apellidos</label>
                                <input type="text" class="form-juandiego" placeholder="Primer Apellido"
                                    wire:model="apellidosconyuge">

                            </div>
                            <div class="col-span-4 mb-1 sm:col-span-1">
                                <label for="lbl_dirconyuge"
                                    class="block text-sm font-bold text-gray-700">Direccion</label>
                                <input type="text" class="form-juandiego" placeholder="Direccion"
                                    wire:model="dirconyuge">
                            </div>
                            <div class="col-span-4 mb-1 sm:col-span-1">
                                <label for="lbl_telconyuge"
                                    class="block text-sm font-bold text-gray-700">Telefono</label>
                                <input wire:model='telconyuge' type="text" class="form-juandiego">

                            </div>
                        </div>
                    </div>
                </div>

                {{-- Pie del Modal footer --}}
                <div class="px-4 py-3 bg-gray-50 sm:px-6 sm:flex sm:flex-row-reverse">
                    @if ($accion == 'crea')
                        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                            <button wire:click.prevent="creaTercero()" type="button"
                                class="justify-center w-full form-botoncrear">
                                Grabar
                                <i wire:loading wire:target="creaTercero" class="ml-2 fa fa-refresh fa-spin"></i>
                            </button>
                        </span>
                    @else
                        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                            @can('actualizar_tercero')
                                <button wire:click.prevent="actualizaTercero()" type="button"
                                    class="justify-center w-full form-botoncrear">
                                    Actualizar
                                    <i wire:loading wire:target="actualizaTercero" class="ml-2 fa fa-refresh fa-spin"></i>
                                </button>
                            @endcan
                        </span>

                    @endif
                    <span class="flex w-full mt-3 rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModal()" type="button"
                            class="justify-center w-full form-botoneliminar">
                            Cancelar
                            <i wire:loading wire:target="closeModal" class="ml-2 fa fa-spinner fa-spin"></i>
                        </button>
                    </span>
                </div>

            </div>
        </div>
    </div>
</div>
