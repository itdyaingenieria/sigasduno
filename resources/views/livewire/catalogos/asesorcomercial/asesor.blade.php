<x-slot name="header">
    <div class="flex text-xs ">
        <h2 class="mr-10 text-xl font-semibold leading-tight text-gray-800">
            .:. Catalogo de Asesores Comerciales .:.</h2>
    </div>

</x-slot>
{{-- Itdyaingenieria --}}
<div class="container w-screen h-12 m-2 mx-auto md:w-auto">
    @if (Session::has('message'))
        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "200",
                "hideDuration": "800",
                "timeOut": "2777",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.success("{{ session('message') }}", "SigasDuno");
        </script>
    @endif
    <div
        class="flex justify-between max-w-4xl p-4 mx-auto mb-2 overflow-hidden bg-gray-100 rounded-lg shadow md:space-x-8 ">
        <div class="mb-1">
            <input wire:model="buscar" type="search" id="txtbuscar" placeholder="Dato a Buscar..."
                class="w-full px-3 py-2 leading-tight text-gray-400 border rounded placeholder-opacity-70 shadow-appearance-none focus:outline-none focus:shadow-outline">
        </div>
        @can('adicionar_cliente')
            <button wire:click='llamarModalCrear()' class="mr-2 form-botoncrear"
                title="Adicionar los Nuevos Datos en Sistema CONDIRAD.">Adicionar
                <i wire:loading wire:target="llamarModalCrear" class="ml-2 fa fa-cog fa-spin"></i>
            </button>
        @endcan
        @if ($isOpen)
            @include('livewire.catalogos.asesorcomercial.create')
        @endif
    </div>
    {{-- Colocamos los valores en la Tabla --}}
    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                <div class="overflow-hidden border-b border-gray-200 shadow sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200 ">
                        <thead class="bg-gray-50">
                            <tr>
                                <th scope="col"
                                    class="px-4 py-3 text-xs font-bold tracking-wider text-center text-gray-500 uppercase ">
                                    #
                                </th>
                                <th scope="col"
                                    class="px-5 py-3 text-xs font-medium tracking-wider text-right text-gray-500 uppercase ">
                                    Identificacion
                                </th>
                                <th scope="col"
                                    class="py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase px-15">
                                    Nombres
                                </th>
                                <th scope="col"
                                    class="py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase px-15">
                                    Apellidos
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase">
                                    Antiguedad
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                                    Tipo
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                                    Fecha Ingreso
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                                    Direccion
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                                    Opciones
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($asesores as $asesor)
                                <tr class="hover:bg-blue-50 ">
                                    <td class="px-4 py-2 whitespace-nowrap">
                                        <div class="text-sm font-bold text-right text-gray-900">
                                            {{ $asesor->asesor_id }}
                                        </div>
                                    </td>
                                    <td class="px-5 py-2">
                                        <div class="text-sm text-right text-blue-900">
                                            {{ $asesor->numeroIdentificacion }}
                                        </div>
                                    </td>
                                    <td class="px-5 py-2">
                                        <div class="text-sm text-right text-blue-900"> {{ $asesor->nombres }}
                                        </div>
                                    </td>
                                    <td class="py-2 px-15">
                                        <div class="text-sm text-gray-900"> {{ $asesor->apellidos }}
                                        </div>
                                    </td>
                                    <td class="px-6 py-2">
                                        <div class="text-sm text-gray-900"> {{ $asesor->antiguedad }}</div>
                                    </td>
                                    <td class="px-6 py-2">
                                        <div class="text-sm text-gray-900"> {{ $asesor->tipo }}</div>
                                    </td>
                                    <td class="px-6 py-2">
                                        <div class="text-sm text-gray-900"> {{ $asesor->fechaIngreso }}</div>
                                    </td>
                                    <td class="px-6 py-2">
                                        <div class="text-sm text-gray-900"> {{ $asesor->direccion }}</div>
                                    </td>
                                    <td>

                                        <button
                                            wire:click="confirmarItemEditar('{{ $asesor->asesor_id }}', '{{ $asesor->asesor_id }}')"
                                            class="form-botoneditar fa fa-pencil-square-o fa-lg"
                                            style="color:rgb(49, 46, 129)" title="Editar Registro de Condirad.">
                                        </button>


                                        <button
                                            onclick="Confirmar('{{ $asesor->asesor_id }}', '{{ $asesor->nombres }}')"
                                            class="form-botoneliminar fa fa-trash-o fa-lg" style="color:rgb(49,46,129)"
                                            title="Eliminar Registro de Condirad.">
                                        </button>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <div wire:loading wire:target="confirmarItemEditar" class="loading">
                            <div class="loading-content"></div>
                        </div>
                    </table>
                    <div class="px-4 py-2 bg-gray-300 border-t border-gray-600 rounded-lg">
                        {{ $asesores->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function Confirmar(id, nombre) {
        let me = this
        swal.fire({
            title: 'Confirmación!!',
            html: "Deseas Eliminar El Registro? => <b>" + id + "--" + nombre + "</b>",
            icon: 'question',
            iconColor: '#6187A4',
            showCancelButton: true,
            confirmButtonColor: '#312E81',
            cancelButtonColor: '#FF0303',
            confirmButtonText: 'Aceptar!',
            cancelButtonText: 'Cancelar',
            footer: '<a href>SIGASDUNO Software</a>',
            background: '#F5F0EA',
            backdrop: `rgba(59, 130, 246,0.3)`

        }).then((result) => {
            if (result.isConfirmed) {
                window.livewire.emit('deleteRow', id) //Enviamos Al Componente
                window.livewire.on('cantidad', cantidad => { //Recibimos Info del Componente
                    if (cantidad <= 0) {
                        swal.fire(
                            'Eliminado!',
                            'El registro=> <b>' + id + "--" + nombre + '</b>, Fue Eliminado!!',
                            'success'
                        )
                    } else {
                        swal.fire(
                            'No Eliminado!',
                            'El registro=> <b>' + id + "--" + nombre +
                            '</b>, Tiene Movimientos en Rutas!!',
                            'error'
                        )
                    }
                })

            }
        })
    };
    //Diego Fernando Yama Andrade
</script>
