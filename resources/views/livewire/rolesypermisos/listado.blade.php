<x-slot name="header">
    <div class="flex text-xs ">
        <h2 class="mr-10 text-xl font-semibold leading-tight text-gray-800">
            .:. Roles Y Permisos de Usuarios .:.</h2>
    </div>

</x-slot>
{{-- Itdyaingenieria --}}
<div class="container w-screen h-12 m-2 mx-auto md:w-auto">
    @if (Session::has('message'))
        <div class="flex items-center bg-blue-900 text-white text-sm font-bold px-4 py-3" role="alert">
            <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <path
                    d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z" />
            </svg>
            <p>"{{ session('message') }}"</p>
        </div>
    @endif
    {{-- Colocamos los valores en la Tabla --}}
    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                <div class="overflow-hidden border-b border-gray-200 shadow sm:rounded-lg">

                    <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4 ">
                        <ul class="nav flex border-b" role="tablist">
                            <li class="-mb-px mr-1">
                                <a class="bg-white inline-block border-l border-t border-r rounded-t py-2 px-4 text-blue-700 font-semibold
                                {{ $tab == 'roles' ? 'active' : '' }}" wire:click="$set('tab','roles')"
                                    data-toggle="pill" href="#roles_content" role="tab">Roles</a>
                            </li>
                            <li class="mr-1">
                                <a class="bg-white inline-block border-l border-t border-r rounded-t py-2 px-4 text-blue-700 font-semibold
                                {{ $tab == 'permisos' ? 'active' : '' }}" wire:click="$set('tab','permisos')"
                                    data-toggle="pill" href=" #permisos_content" role="tab">Permisos</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            @if ($tab == 'roles')
                                @include('livewire.rolesypermisos.roles')
                            @else
                                @include('livewire.rolesypermisos.permisos')
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function showRole(role) {
        var data = JSON.parse(role)
        $('#rolName').val(data['name'])
        $('#rolId').val(data['id'])
    }

    function clearRoleSelected() {
        $('#rolName').val('')
        $('#rolName').focus()
        $('#rolId').val(0)
    }

    function showPermission(permission) {
        var data = JSON.parse(permission)
        $('#permissionName').val(data['name'])
        $('#permissionId').val(data['id'])
    }

    function clearPermissionSelected() {
        $('#permissionName').val('')
        $('#permissionName').focus()
        $('#permissionId').val(0)
    }



    function Confirm(id, eventName) {
        swal.fire({
            title: 'Confirmación!!',
            html: "Deseas Eliminar El Registro? => <b>" + id + "</b>",
            icon: 'question',
            iconColor: '#6187A4',
            showCancelButton: true,
            confirmButtonColor: '#312E81',
            cancelButtonColor: '#FF0303',
            confirmButtonText: 'Aceptar!',
            cancelButtonText: 'Cancelar',
            footer: '<a href>ITDyaingenieria Software</a>',
            background: '#F5F0EA',
            backdrop: `rgba(59, 130, 246,0.3)`

        }).then((result) => {
            if (result.isConfirmed) {
                window.livewire.emit(eventName, id),
                    swal.fire(
                        'Eliminado!',
                        'El registro=> ' + id + ', Fue Eliminado!!',
                        'success'
                    )
                $('#rolName').val('')
                $('#rolId').val(0)
                $('#permissionName').val('')
                $('#permissionId').val(0)
            }
        })
    }

    function AsignarRoles() {
        var rolesList = []
        $('#tblRoles').find('input[type=checkbox]:checked').each(function() {
            rolesList.push($(this).attr('data-name'))
        })
        if (rolesList.length < 1) {
            toastr.error('', 'Seleccione Al Menos Un Rol');
            return;
        } else if ($('#userId option:selected').val() == 'Seleccionar') {
            toastr.error('', 'Seleccione un Usuario para Asignar Rol');
            return;
        }

        window.livewire.emit('AsignarRoles', rolesList)
    }

    function AsignarPermisos() {
        var permisosList = []
        $('#tblPermisos').find('input[type=checkbox]:checked').each(function() {
            permisosList.push($(this).attr('data-name'))
        })
        if (permisosList.length < 1) {
            toastr.error('', 'Seleccione Al Menos Un Permiso');
            return;
        }
        if ($('#rolId option:selected').val() == 'Seleccionar') {
            toastr.error('', 'seleccione un Rol para asignar Permisos');
            return;
        }

        window.livewire.emit('AsignarPermisos', permisosList, $('#rolId option:selected').val())
    }

    document.addEventListener('DOMContentLoaded', function() {
        window.livewire.on('activarTab', tabName => {
            var tab = "[href='" + tabName + "']"
            $(tab).tab('show')
        })

        $('body').on('click', '.check-all', function() {
            var state = $(this).is(':checked') ? true : false

            $('#tblPermisos').find('input[type=checkbox]').each(function(e) {
                $(this).prop('checked', state)
            })

        })
    })


    //Msc Ing Diego Fernando Yama Andrade
</script>
