<div class="tab-pane fade {{ $tab == 'roles' ? 'show active' : '' }}" id="roles_content" role="tabpanel">

    <div class="grid grid-rows-4 grid-flow-col gap-1">
        <div class="md:grid md:grid-cols-4 md:gap-1">
            <div class="mt-1 md:col-span-2">
                <div class="overflow-hidden shadow sm:rounded-md">
                    <h4 class="px-4 py-1 font-bold bg-indigo-900 text-indigo-50">
                        Listado Roles de Seguridad.:.
                    </h4>
                    <div class="m-1 grid grid-cols-2 gap-3">

                        <div class="mb-1 col-span-2 sm:col-span-2">
                            <div class="relative text-gray-600 focus-within:text-gray-400">
                                <span class="absolute inset-y-0 left-0 flex items-center pl-2">
                                    @can('adicionar_rol')
                                        <button type="submit"
                                            class="p-1 focus:outline-none focus:shadow-outline fa fa-save fa-lg"
                                            wire:click="$emit('crearRol',$('#rolName').val(),$('#rolId').val())">
                                        </button>
                                    @endcan
                                </span>
                                <input type="search" id="rolName"
                                    class="py-2 text-sm text-white bg-blue-900 rounded-md pl-10 focus:outline-none focus:bg-white focus:text-blue-900"
                                    placeholder="Buscar..." autocomplete="off">
                                <input type="hidden" id="rolId">
                            </div>

                        </div>
                        <div class="mb-1 col-span-2 sm:col-span-2">
                            <table id="tblRoles" class="text-left w-full ">
                                <thead class="bg-blue-100 flex text-blue-900 w-full rounded-xl">
                                    <tr class="flex w-full mb-2">
                                        <th class="p-1 w-1/2 text-center">DESCRIPCION</th>
                                        <th class="p-1 w-1/2 text-center">USUARIOS</th>
                                        <th class="p-1 w-1/2 text-center">ACCIONES</th>
                                        <th class="p-1 w-1/2 text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody class="bg-grey-light flex flex-col items-center  overflow-y-scroll w-full"
                                    style="height: 30vh;">
                                    @foreach ($roles as $r)
                                        <tr class="flex w-full hover:bg-indigo-50">
                                            <td class="p-1 w-1/2 border-b  border-indigo-200 text-sm text-center">
                                                {{ $r->name }}</td>
                                            <td class="p-1 w-1/2 border-b  border-indigo-200 text-sm text-center">
                                                {{ \App\Models\User::role($r->name)->count() }}</td>
                                            <td class="p-1 w-1/2 border-b  border-indigo-200 text-sm text-center">
                                                @can('editar_rol')
                                                    <span style="cursor: pointer" onclick="showRole('{{ $r }}')">
                                                        <i class="fa fa-pencil-square-o fa-lg"></i>
                                                    </span>
                                                @endcan
                                                @can('eliminar_rol')
                                                    @if (\App\Models\User::role($r->name)->count() <= 0)
                                                        <a href="javascript:void(0)"
                                                            onclick="Confirm('{{ $r->id }}','destroyRol')"
                                                            title="Eliminar Role"><i class="fa fa-trash fa-lg"></i>
                                                    @endif
                                                @endcan
                                            </td>
                                            <td class="p-1 w-1/2 border-b  border-indigo-200 text-sm text-center">
                                                <div class="n-check" id="divRoles">
                                                    <label class="">
                                                        <input data-name="{{ $r->name }}"
                                                            {{ $r->checked == 1 ? 'checked' : '' }} type="checkbox"
                                                            class="checkbox-rol">
                                                        <span>Asignar</span>
                                                    </label>

                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-1 px-2 md:col-span-2">
                <div class="overflow-hidden shadow sm:rounded-md">
                    <h4 class="px-4 py-1 font-bold bg-indigo-900 text-indigo-50">
                        Usuarios registrados .:.
                    </h4>
                    <div class="col-span-2 mb-1 sm:col-span-2 ">
                        <label for="lbl_userSelected" class="m-2 block text-sm font-bold text-gray-700 ">Elegir
                            Usuario</label>
                        <select wire:model="userSelected" id="userId" class="m-2 text-lg flex form-juandiego">
                            <option value="Seleccionar" selected>Seleccionar</option>
                            @foreach ($usuarios as $u)
                                <option value={{ $u->id }}>
                                    {{ $u->id }}-{{ $u->name }}
                                </option>
                            @endforeach
                        </select>

                    </div>
                    <div class=" col-span-2 mb-1 sm:col-span-2 ">
                        <span class=" m-2 flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                            @can('asignar_rol')
                                <button onclick="AsignarRoles()" type="button"
                                    class="justify-center w-full form-botoncrear">
                                    Asignar Roles
                                    <i wire:loading wire:target="AsignarRoles" class="ml-2 fa fa-refresh fa-spin"></i>
                                </button>
                            @endcan
                        </span>
                    </div>
                </div>
            </div>

        </div>
    </div>
