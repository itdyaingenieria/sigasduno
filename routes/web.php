<?php

use Illuminate\Support\Facades\Route;

use App\Http\Livewire\Catalogos\clientes\ClientesComponente;
//Catalogos de vehiculos
use App\Http\Livewire\Catalogos\vehiculos\VehiculosComponente;
//Catalogos de terceros/codperso
use App\Http\Livewire\Catalogos\codperso\CodpersoComponente;
//Catalogos de terceros/codperso
use App\Http\Livewire\Catalogos\asesorcomercial\AsesorComercialComponente;
// movimientos Componentes
use App\Http\Livewire\movimientos\controlRuta\ControlRutaComponente;
use App\Http\Livewire\Rolesypermisos\RolespermisosComponente;
use App\Http\Livewire\consultaClientes\rutas\ConsultaRutaComponente;
use App\Http\Livewire\consultaClientes\asignacion\ClientesAsignacionComponente;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/sigasduno/clientes', ClientesComponente::class)->name('cat_clientes');
//Catalogo de Vehiculos
Route::get('/sigasduno/vehiculos', VehiculosComponente::class)->name('cat_vehiculos');
//Catalogo de Terceros-codperso
Route::get('/sigasduno/codperso', CodpersoComponente::class)->name('cat_codperso');
//Catalogo de Asesores Comerciales-
Route::get('/sigasduno/asesores', AsesorComercialComponente::class)->name('cat_asesor');

// Menu Movimientos
Route::get('/sigasduno/Rutas', ControlRutaComponente::class)->name('mov_controlRuta');


//Menu de Roles Y permisos
Route::get('/sigasduno/rolespermisos', RolespermisosComponente::class)->name('gest_rolespermisos');


