-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sigasduno
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asesorcomercial`
--

DROP TABLE IF EXISTS `asesorcomercial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asesorcomercial` (
  `asesor_id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdentificacion` varchar(15) DEFAULT NULL,
  `nombres` varchar(45) DEFAULT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `antiguedad` varchar(2) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`asesor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asesorcomercial`
--

LOCK TABLES `asesorcomercial` WRITE;
/*!40000 ALTER TABLE `asesorcomercial` DISABLE KEYS */;
INSERT INTO `asesorcomercial` VALUES (1,'12585569','SEGUNDO','YAMA CEBALLOS','25','PLANTA','1992-05-05','MADRID CASA 8');
/*!40000 ALTER TABLE `asesorcomercial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clientes` (
  `cliente_id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroIdenficacion` varchar(45) DEFAULT NULL,
  `nombres` varchar(55) DEFAULT NULL,
  `apellidos` varchar(55) DEFAULT NULL,
  `direccion` varchar(65) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`cliente_id`),
  KEY `fk_clientes_empresa_idx` (`empresa_id`),
  CONSTRAINT `fk_clientes_empresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`empresa_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'27253145','AMANDA','MORA HUERTAS','CRA 65 # 36 45','321255125','AMANDA@GMAIL.COM',1),(2,'41487255','GLORIA DEL ROSARIO','MENESES FLOREZ','BARRIO LIMEDEC CASA 4 MANZANA 78','3013407288','gloria_meneses@gmail.com',1);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conductores`
--

DROP TABLE IF EXISTS `conductores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `conductores` (
  `conductor_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(55) DEFAULT NULL,
  `apellidos` varchar(55) DEFAULT NULL,
  `direccion` varchar(75) DEFAULT NULL,
  `tipoLicencia` varchar(5) DEFAULT NULL,
  `numeroLicencia` varchar(15) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `vehiculo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`conductor_id`),
  KEY `fk_conductores_vehiculo_idx` (`vehiculo_id`),
  CONSTRAINT `fk_conductores_vehiculo` FOREIGN KEY (`vehiculo_id`) REFERENCES `vehiculos` (`vehiculo_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='			';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conductores`
--

LOCK TABLES `conductores` WRITE;
/*!40000 ALTER TABLE `conductores` DISABLE KEYS */;
INSERT INTO `conductores` VALUES (1,'DIEGO FERNANDO','YAMÁ ANDRADE','COLINA NORTE MAZ G CASA 11','C2','87101225','df60@misena.edu.co','3013407265',1),(3,'JUAN FERNANDO','YAMÁ MORA','COLINA AZUL MAZ K CASA 11','C2','87181225','df61@misena.edu.co','3053407266',3);
/*!40000 ALTER TABLE `conductores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empresa` (
  `empresa_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoid` varchar(3) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `direccion` varchar(55) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `email` varchar(65) DEFAULT NULL,
  `sitioweb` varchar(55) DEFAULT NULL,
  `responsabildadfiscal` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`empresa_id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'NIT','EMPRESA GAS D UNO','MADRID CUNDINAMARCA','7889588','gerencia@gasduno.com','www.gasduno.com','no responsable iva');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kilometrajes`
--

DROP TABLE IF EXISTS `kilometrajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kilometrajes` (
  `kilometraje_id` int(11) NOT NULL AUTO_INCREMENT,
  `fechaKilometraje` date DEFAULT NULL,
  `combustibleConsumido` varchar(45) DEFAULT NULL,
  `vehiculo_id` int(11) NOT NULL,
  PRIMARY KEY (`kilometraje_id`),
  KEY `fk_kilometrajes_vehiculo_idx` (`vehiculo_id`),
  CONSTRAINT `fk_kilometrajes_vehiculo` FOREIGN KEY (`vehiculo_id`) REFERENCES `vehiculos` (`vehiculo_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kilometrajes`
--

LOCK TABLES `kilometrajes` WRITE;
/*!40000 ALTER TABLE `kilometrajes` DISABLE KEYS */;
INSERT INTO `kilometrajes` VALUES (1,'2021-09-02','25',1);
/*!40000 ALTER TABLE `kilometrajes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),(4,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2019_12_14_000001_create_personal_access_tokens_table',1),(6,'2021_05_11_122358_create_sessions_table',1),(7,'2021_06_24_112737_create_permission_tables',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,'App\\Models\\User',1);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `obtenerempresa`
--

DROP TABLE IF EXISTS `obtenerempresa`;
/*!50001 DROP VIEW IF EXISTS `obtenerempresa`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `obtenerempresa` AS SELECT 
 1 AS `empresa_id`,
 1 AS `tipoid`,
 1 AS `nombre`,
 1 AS `direccion`,
 1 AS `telefono`,
 1 AS `email`,
 1 AS `sitioweb`,
 1 AS `responsabildadfiscal`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedidos`
--

DROP TABLE IF EXISTS `pedidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pedidos` (
  `pedido_id` int(11) NOT NULL AUTO_INCREMENT,
  `fechaPedido` date DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `asesor_id` int(11) DEFAULT NULL,
  `ruta_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`pedido_id`),
  KEY `fk_pedidos_cliente_idx` (`cliente_id`),
  KEY `fk_pedidos_ruta_idx` (`ruta_id`),
  KEY `fk_pedidos_asesor_idx` (`asesor_id`),
  CONSTRAINT `fk_pedidos_asesor` FOREIGN KEY (`asesor_id`) REFERENCES `asesorcomercial` (`asesor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedidos_cliente` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`cliente_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedidos_ruta` FOREIGN KEY (`ruta_id`) REFERENCES `rutas` (`ruta_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedidos`
--

LOCK TABLES `pedidos` WRITE;
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
INSERT INTO `pedidos` VALUES (1,'2021-09-01',1,1,1);
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'adicionar_vehiculo','web','2021-06-26 01:31:11','2021-06-26 15:07:34'),(2,'editar_vehiculo','web','2021-06-26 02:46:00','2021-06-26 15:07:49'),(4,'actualizar_vehiculo','web','2021-06-26 15:08:02','2021-06-26 15:08:02'),(5,'eliminar_vehiculo','web','2021-06-26 15:08:14','2021-06-26 15:08:14'),(6,'menu_vehiculos','web','2021-06-26 15:08:26','2021-06-26 15:08:26'),(7,'adicionar_tercero','web','2021-06-26 15:09:55','2021-06-26 15:09:55'),(8,'editar_tercero','web','2021-06-26 15:10:05','2021-06-26 15:10:05'),(9,'actualizar_tercero','web','2021-06-26 15:10:15','2021-06-26 15:10:15'),(10,'eliminar_tercero','web','2021-06-26 15:10:28','2021-06-26 15:10:28'),(11,'menu_terceros','web','2021-06-26 15:10:42','2021-06-26 15:10:42'),(12,'adicionar_rol','web','2021-06-26 15:11:42','2021-06-26 15:11:42'),(13,'editar_rol','web','2021-06-26 15:11:52','2021-06-26 15:11:52'),(14,'eliminar_rol','web','2021-06-26 15:12:03','2021-06-26 15:12:03'),(15,'asignar_rol','web','2021-06-26 15:12:14','2021-06-26 15:12:14'),(16,'menu_roles','web','2021-06-26 15:12:24','2021-06-26 15:12:24'),(17,'adicionar_permiso','web','2021-06-26 15:12:36','2021-06-26 15:12:36'),(18,'editar_permiso','web','2021-06-26 15:12:47','2021-06-26 15:12:47'),(19,'eliminar_permiso','web','2021-06-26 15:12:58','2021-06-26 15:12:58'),(20,'asignar_permiso','web','2021-06-26 15:13:08','2021-06-26 15:13:08'),(21,'adicionar_hoja','web','2021-06-26 15:33:00','2021-06-26 15:33:00'),(22,'editar_hoja','web','2021-06-26 15:33:17','2021-06-26 15:33:17'),(23,'actualizar_hoja','web','2021-06-26 15:33:31','2021-06-26 15:33:31'),(24,'eliminar_hoja','web','2021-06-26 15:33:43','2021-06-26 15:33:43'),(25,'listar_hoja','web','2021-06-26 15:33:54','2021-06-26 15:33:54'),(26,'imprimir_hoja','web','2021-06-26 15:34:06','2021-06-26 15:34:06'),(27,'menu_hojas','web','2021-06-26 15:34:16','2021-06-26 15:34:16'),(28,'adicionar_logistico','web','2021-06-26 15:36:02','2021-06-26 15:36:02'),(29,'editar_logistico','web','2021-06-26 15:36:13','2021-06-26 15:36:13'),(30,'actualizar_logistico','web','2021-06-26 15:36:22','2021-06-26 15:36:22'),(31,'eliminar_logistico','web','2021-06-26 15:36:32','2021-06-26 15:36:32'),(32,'firmar_logistico','web','2021-06-26 15:36:44','2021-06-26 15:36:44'),(33,'imprimir_logistico','web','2021-06-26 15:36:55','2021-06-26 15:36:55'),(34,'menu_logisticos','web','2021-06-26 15:37:07','2021-06-26 15:37:07'),(35,'adicionar_precinto','web','2021-06-26 15:37:18','2021-06-26 15:37:18'),(36,'editar_precinto','web','2021-06-26 15:37:30','2021-06-26 15:37:30'),(37,'actualizar_precinto','web','2021-06-26 15:37:40','2021-06-26 15:37:40'),(38,'eliminar_precinto','web','2021-06-26 15:37:49','2021-06-26 15:37:49'),(39,'reporte_precinto','web','2021-06-26 15:38:00','2021-06-26 15:38:00'),(40,'menu_precintos','web','2021-06-26 15:38:10','2021-06-26 15:38:10'),(41,'adicionar_ruta','web','2021-06-26 15:39:50','2021-06-26 15:39:50'),(42,'editar_ruta','web','2021-06-26 15:40:05','2021-06-26 15:40:05'),(43,'actualizar_ruta','web','2021-06-26 15:40:14','2021-06-26 15:40:14'),(44,'eliminar_ruta','web','2021-06-26 15:40:24','2021-06-26 15:40:24'),(45,'reporterastreo_ruta','web','2021-06-26 15:40:54','2021-06-26 15:40:54'),(46,'agregarrastreo_ruta','web','2021-06-26 15:41:05','2021-06-26 15:41:05'),(47,'editarrastreo_ruta','web','2021-06-26 15:41:16','2021-06-26 15:41:16'),(48,'eliminarrastreo_ruta','web','2021-06-26 15:41:29','2021-06-26 15:41:29'),(49,'actualizarrastreo_ruta','web','2021-06-26 15:41:42','2021-06-26 15:41:42'),(50,'verdatosgps_ruta','web','2021-06-26 15:41:52','2021-06-26 15:41:52'),(51,'reporte_rutas','web','2021-06-26 15:42:06','2021-06-26 15:42:06'),(52,'menu_rutas','web','2021-06-26 15:42:16','2021-06-26 15:42:16'),(53,'menu_clientes','web','2021-07-08 15:49:56','2021-07-08 15:49:56'),(54,'adicionar_cliente','web','2021-07-08 16:06:27','2021-07-08 16:06:27'),(55,'editar_cliente','web','2021-07-08 16:24:19','2021-07-08 16:24:19'),(56,'eliminar_cliente','web','2021-07-08 16:24:29','2021-07-08 16:24:29'),(57,'actualizar_cliente','web','2021-07-08 20:25:49','2021-07-08 20:25:49'),(58,'autorizar_hoja','web','2021-07-10 16:02:32','2021-07-10 16:02:32'),(59,'actualizar_hoja_seguridad','web','2021-07-10 22:16:58','2021-07-10 22:16:58'),(60,'cargar_hoja_seguridad','web','2021-07-12 12:56:52','2021-07-12 12:56:52'),(61,'autorizar_habilitacion','web','2021-07-12 16:44:01','2021-07-12 16:44:01'),(62,'actualizar_hoja_habilita','web','2021-07-13 14:03:10','2021-07-13 14:03:10'),(63,'menu_asignacion','web','2021-07-28 23:44:45','2021-07-28 23:44:45'),(64,'adicionar_asignacion','web','2021-07-28 23:46:28','2021-07-28 23:46:28'),(65,'editar_asignacion','web','2021-07-28 23:46:38','2021-07-28 23:46:38'),(66,'eliminar_asignacion','web','2021-07-28 23:46:51','2021-07-28 23:46:51'),(67,'menu_rutaxcliente','web','2021-07-28 23:50:57','2021-07-28 23:50:57');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,1),(2,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Superusuario','web',NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rutas`
--

DROP TABLE IF EXISTS `rutas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rutas` (
  `ruta_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(65) DEFAULT NULL,
  `origen` varchar(45) DEFAULT NULL,
  `destino` varchar(45) DEFAULT NULL,
  `conductor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ruta_id`),
  KEY `fk_rutas_conductor_idx` (`conductor_id`),
  CONSTRAINT `fk_rutas_conductor` FOREIGN KEY (`conductor_id`) REFERENCES `conductores` (`conductor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rutas`
--

LOCK TABLES `rutas` WRITE;
/*!40000 ALTER TABLE `rutas` DISABLE KEYS */;
INSERT INTO `rutas` VALUES (1,'RUTA NUMERO 1 DESTACADA','BARRIO ALCAZAR','BARRIO NORMANDIA',1);
/*!40000 ALTER TABLE `rutas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('qBJipP7M6N2WK6brAdXT0jEqntYPGgiiEuT8dZPu',1,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36','YTo2OntzOjY6Il90b2tlbiI7czo0MDoiRjloNGFoQWxRcWNJRmdWSWpTM2hqVVVvT3dKcFFqZ0VCZkpubFZoZyI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9zaWdhc2R1bm8vcm9sZXNwZXJtaXNvcyI7fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE7czoxNzoicGFzc3dvcmRfaGFzaF93ZWIiO3M6NjA6IiQyeSQxMCQ3T0hJVVAwa0NrRDc4V1djUE5BWjNlRWdpME5oM2taYTR5aDh0dkpYTDVrbS9YcGxiSW92aSI7czoyMToicGFzc3dvcmRfaGFzaF9zYW5jdHVtIjtzOjYwOiIkMnkkMTAkN09ISVVQMGtDa0Q3OFdXY1BOQVozZUVnaTBOaDNrWmE0eWg4dHZKWEw1a20vWHBsYklvdmkiO30=',1638382843);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) unsigned DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Diego Fernando Yamá Andrade','admin@sigasduno.com',NULL,'$2y$10$7OHIUP0kCkD78WWcPNAZ3eEgi0Nh3kZa4yh8tvJXL5km/XplbIovi',NULL,NULL,NULL,NULL,NULL,NULL,'2021-12-01 18:17:38');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiculos`
--

DROP TABLE IF EXISTS `vehiculos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehiculos` (
  `vehiculo_id` int(11) NOT NULL AUTO_INCREMENT,
  `placa` varchar(7) DEFAULT NULL,
  `marca` varchar(45) DEFAULT NULL,
  `modelo` varchar(4) DEFAULT NULL,
  `tipocombustible` varchar(45) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`vehiculo_id`),
  UNIQUE KEY `placa_UNIQUE` (`placa`),
  KEY `fk_vehiculos_empresa_idx` (`empresa_id`),
  CONSTRAINT `fk_vehiculos_empresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`empresa_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiculos`
--

LOCK TABLES `vehiculos` WRITE;
/*!40000 ALTER TABLE `vehiculos` DISABLE KEYS */;
INSERT INTO `vehiculos` VALUES (1,'XEJ494','TOYOTA PRADO','2005','GASOLINA',1),(3,'XEJ495','CHEVROLET','2006','GASOLINA',1),(4,'XEJ496','CHEVROLET LUV','2016','GASOLINA',1);
/*!40000 ALTER TABLE `vehiculos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `obtenerempresa`
--

/*!50001 DROP VIEW IF EXISTS `obtenerempresa`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `obtenerempresa` AS select `empresa`.`empresa_id` AS `empresa_id`,`empresa`.`tipoid` AS `tipoid`,`empresa`.`nombre` AS `nombre`,`empresa`.`direccion` AS `direccion`,`empresa`.`telefono` AS `telefono`,`empresa`.`email` AS `email`,`empresa`.`sitioweb` AS `sitioweb`,`empresa`.`responsabildadfiscal` AS `responsabildadfiscal` from `empresa` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-01 13:22:26
